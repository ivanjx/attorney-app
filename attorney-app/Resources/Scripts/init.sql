﻿-- All files go here.
CREATE TABLE files (
    id VARCHAR(32) NOT NULL,
    original_name VARCHAR(254) NOT NULL,
    PRIMARY KEY (id)
);

-- Groups list of contacts.
CREATE TABLE contact_groups (
    id INT NOT NULL,
    value VARCHAR(32) NOT NULL,
    PRIMARY KEY (id)
);

-- The contacts list.
CREATE TABLE contacts (
    id VARCHAR(32) NOT NULL,
    name VARCHAR(40) NOT NULL,
    is_male INT NOT NULL,
    birth_date DATETIME,
    ssn VARCHAR(20), -- Social security number.
    address TEXT,
    county VARCHAR(50),
    state VARCHAR(50),
    zip_code VARCHAR(10),
    home_phone VARCHAR(20),
    work_phone VARCHAR(20),
    cell_phone VARCHAR(20),
    fax VARCHAR(20),
    email VARCHAR(254),
    group_id INT NOT NULL,
    photo_id VARCHAR(32),
    PRIMARY KEY (id),
    FOREIGN KEY (group_id) REFERENCES contact_groups(id) ON DELETE CASCADE
    --FOREIGN KEY (photo_id) REFERENCES files(id) -- If nullable, dont set as FK!
);

-- The case_types table.
CREATE TABLE case_types (
    id INT NOT NULL,
    value VARCHAR(40) NOT NULL,
    PRIMARY KEY (id)
);

-- The cases table.
CREATE TABLE cases (
    id VARCHAR(32) NOT NULL, -- AKA case number
    name VARCHAR(40) NOT NULL,
    type_id INT NOT NULL,
    date_retainer DATETIME,
    date_accident DATETIME,
    date_max DATETIME,
    date_app_deadline DATETIME,
    description TEXT,
    trial_venue TEXT,
    liability_limits TEXT,
    is_done INT NOT NULL,
    time_added DATETIME NOT NULL,
    PRIMARY KEY (id)
    FOREIGN KEY (type_id) REFERENCES case_types(id) ON DELETE CASCADE
);

-- Link of cases and contacts.
CREATE TABLE case_contact_links (
    case_id VARCHAR(32) NOT NULL,
    contact_id VARCHAR(32) NOT NULL,
    FOREIGN KEY (case_id) REFERENCES cases(id) ON DELETE CASCADE,
    FOREIGN KEY (contact_id) REFERENCES contacts(id) ON DELETE CASCADE
);

-- Link of cases and documents.
CREATE TABLE case_file_links (
    case_id VARCHAR(32) NOT NULL,
    file_id VARCHAR(32) NOT NULL,
    description VARCHAR(50) NOT NULL,
    FOREIGN KEY (case_id) REFERENCES cases(id) ON DELETE CASCADE,
    FOREIGN KEY (file_id) REFERENCES files(id) ON DELETE CASCADE
);

-- Cases notes table.
CREATE TABLE notes (
    id VARCHAR(32) NOT NULL,
    body TEXT NOT NULL,
    time_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

-- Link table between notes and cases.
CREATE TABLE case_note_links (
    case_id VARCHAR(32) NOT NULL,
    note_id VARCHAR(32) NOT NULL,
    FOREIGN KEY (case_id) REFERENCES cases(id) ON DELETE CASCADE,
    FOREIGN KEY (note_id) REFERENCES notes(id) ON DELETE CASCADE
);

-- List of bills (USD).
CREATE TABLE billings (
    id VARCHAR(32) NOT NULL,
    name VARCHAR(50) NOT NULL,
    case_settle_date DATETIME NOT NULL,
    liens_amount DOUBLE NOT NULL,
    attorney_compensation_amount DOUBLE NOT NULL,
    client_compensation_amount DOUBLE NOT NULL,
    total DOUBLE NOT NULL,
    is_paid INT NOT NULL,
    PRIMARY KEY (id)
);

-- Links between billing and the files.
CREATE TABLE billing_file_links (
    expense_name VARCHAR(40) NOT NULL,
    amount DOUBLE NOT NULL,
    billing_id VARCHAR(32) NOT NULL,
    file_id VARCHAR(32) NOT NULL,
    FOREIGN KEY (billing_id) REFERENCES billings(id) ON DELETE CASCADE,
    FOREIGN KEY (file_id) REFERENCES files(id) ON DELETE CASCADE
);

-- Template files (dotx file) fields.
CREATE TABLE templates (
    id VARCHAR(32) NOT NULL,
    description TEXT NOT NULL,
    file_id VARCHAR(32) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (file_id) REFERENCES files(id) ON DELETE CASCADE
);

-- List of fields used by templates.
CREATE TABLE template_fields (
    template_id VARCHAR(32) NOT NULL,
    field_name VARCHAR(50) NOT NULL,
    FOREIGN KEY (template_id) REFERENCES templates(id) ON DELETE CASCADE
);

-- Something she will do with her clients.
CREATE TABLE tasks (
    id VARCHAR(32) NOT NULL,
    details TEXT NOT NULL,
    contact_id VARCHAR(32),
    time_start DATETIME NOT NULL,
    time_end DATETIME NOT NULL,
    is_done INT NOT NULL,
    PRIMARY KEY (id)
);

-- Counties in new york. (Loose)
CREATE TABLE ny_counties (
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (name)
);

-- Filling tables.
-- Filling ny_counties.
INSERT INTO ny_counties(name) VALUES('ALBANY');
INSERT INTO ny_counties(name) VALUES('ALLEGANY');
INSERT INTO ny_counties(name) VALUES('BRONX');
INSERT INTO ny_counties(name) VALUES('BROOME');
INSERT INTO ny_counties(name) VALUES('CATTARAUGUS');
INSERT INTO ny_counties(name) VALUES('CAYUGA');
INSERT INTO ny_counties(name) VALUES('CHAUTAUQUA');
INSERT INTO ny_counties(name) VALUES('CHEMUNG');
INSERT INTO ny_counties(name) VALUES('CHENANGO');
INSERT INTO ny_counties(name) VALUES('CLINTON');
INSERT INTO ny_counties(name) VALUES('COLUMBIA');
INSERT INTO ny_counties(name) VALUES('CORTLAND');
INSERT INTO ny_counties(name) VALUES('DELAWARE');
INSERT INTO ny_counties(name) VALUES('DUTCHNESS');
INSERT INTO ny_counties(name) VALUES('ERIE');
INSERT INTO ny_counties(name) VALUES('ESSEX');
INSERT INTO ny_counties(name) VALUES('FRANKLIN');
INSERT INTO ny_counties(name) VALUES('FULTON');
INSERT INTO ny_counties(name) VALUES('GENESEE');
INSERT INTO ny_counties(name) VALUES('GREENE');
INSERT INTO ny_counties(name) VALUES('HAMILTON');
INSERT INTO ny_counties(name) VALUES('HERKIMER');
INSERT INTO ny_counties(name) VALUES('JEFFERSON');
INSERT INTO ny_counties(name) VALUES('KINGS');
INSERT INTO ny_counties(name) VALUES('LEWIS');
INSERT INTO ny_counties(name) VALUES('LIVINGSTON');
INSERT INTO ny_counties(name) VALUES('MADISON');
INSERT INTO ny_counties(name) VALUES('MONROE');
INSERT INTO ny_counties(name) VALUES('MONTGOMERY');
INSERT INTO ny_counties(name) VALUES('NASSAU');
INSERT INTO ny_counties(name) VALUES('NEW YORK');
INSERT INTO ny_counties(name) VALUES('NIAGARA');
INSERT INTO ny_counties(name) VALUES('ONEIDA');
INSERT INTO ny_counties(name) VALUES('ONONDAGA');
INSERT INTO ny_counties(name) VALUES('ONTARIO');
INSERT INTO ny_counties(name) VALUES('ORANGE');
INSERT INTO ny_counties(name) VALUES('ORLEANS');
INSERT INTO ny_counties(name) VALUES('OSWEGO');
INSERT INTO ny_counties(name) VALUES('OTSEGO');
INSERT INTO ny_counties(name) VALUES('PUTNAM');
INSERT INTO ny_counties(name) VALUES('QUEENS');
INSERT INTO ny_counties(name) VALUES('RENSSELAER');
INSERT INTO ny_counties(name) VALUES('RICHMOND');
INSERT INTO ny_counties(name) VALUES('ROCKLAND');
INSERT INTO ny_counties(name) VALUES('SAINT LAWRENCE');
INSERT INTO ny_counties(name) VALUES('SARATOGA');
INSERT INTO ny_counties(name) VALUES('SCHENECTADY');
INSERT INTO ny_counties(name) VALUES('SENECA');
INSERT INTO ny_counties(name) VALUES('STEUBEN');
INSERT INTO ny_counties(name) VALUES('SUFFOLK');
INSERT INTO ny_counties(name) VALUES('SULLIVAN');
INSERT INTO ny_counties(name) VALUES('TIOGA');
INSERT INTO ny_counties(name) VALUES('TOMPKINS');
INSERT INTO ny_counties(name) VALUES('ULSTER');
INSERT INTO ny_counties(name) VALUES('WARREN');
INSERT INTO ny_counties(name) VALUES('WASHINGTON');
INSERT INTO ny_counties(name) VALUES('WAYNE');
INSERT INTO ny_counties(name) VALUES('WESTCHESTER');
INSERT INTO ny_counties(name) VALUES('WYOMING');
INSERT INTO ny_counties(name) VALUES('YATES');

-- Filling contact_groups.
INSERT INTO contact_groups(id, value) VALUES(1, 'PLAINTIFF');
INSERT INTO contact_groups(id, value) VALUES(2, 'DEFENDANT');
INSERT INTO contact_groups(id, value) VALUES(3, 'WITNESS');