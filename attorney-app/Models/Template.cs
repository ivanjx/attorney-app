﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class Template
    {
        public string ID { get; set; }

        public string FileID { get; set; }

        public string FileName { get; set; }

        public string Description { get; set; }


        public override bool Equals(object obj)
        {
            Template comp;
            if ((comp = obj as Template) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
