﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class CaseDocument
    {
        public string FileID { get; set; }

        public string OriginalName { get; set; }

        public string OriginalPath { get; set; } // Not mandatory!

        public long Size { get; set; }

        public string Description { get; set; }


        public override bool Equals(object obj)
        {
            CaseDocument comp;
            if ((comp = obj as CaseDocument) != null)
            {
                return FileID == comp.FileID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
