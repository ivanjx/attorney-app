﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class ContactGroup
    {
        public int ID { get; set; }

        public string Name { get; set; }


        public override bool Equals(object obj)
        {
            ContactGroup comp;
            if ((comp = obj as ContactGroup) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
