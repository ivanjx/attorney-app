﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class AttorneyTask
    {
        public string ID { get; set; }

        public string Details { get; set; }

        public Contact Contact { get; set; }

        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }

        public bool IsDone { get; set; }


        public override bool Equals(object obj)
        {
            AttorneyTask comp;
            if ((comp = obj as AttorneyTask) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
