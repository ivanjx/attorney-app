﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class CaseNote
    {
        public string ID { get; set; }

        public string Body { get; set; }

        public DateTime TimeCreated { get; set; }


        public override bool Equals(object obj)
        {
            CaseNote comp;
            if ((comp = obj as CaseNote) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
