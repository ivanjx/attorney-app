﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class BillingDocument
    {
        public string BillingID { get; set; }

        public string ExpenseName { get; set; }

        public double Amount { get; set; }

        public string FileID { get; set; }

        public string FileName { get; set; }

        public string FileOriginalPath { get; set; } // Not mandatory!

        public long FileSize { get; set; }


        public override bool Equals(object obj)
        {
            BillingDocument comp;
            if ((comp = obj as BillingDocument) != null)
            {
                return BillingID == comp.BillingID && FileID == comp.FileID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
