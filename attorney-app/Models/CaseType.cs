﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class CaseType
    {
        public int ID { get; set; }

        public string Name { get; set; }


        public override bool Equals(object obj)
        {
            CaseType comp;
            if ((comp = obj as CaseType) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
