﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class Contact
    {
        // Personal info.
        public string ID { get; set; }

        public string Name { get; set; }

        public bool IsMale { get; set; }

        public DateTime BirthDate { get; set; }

        public ContactGroup Group { get; set; }

        public string SSN { get; set; }

        public string PhotoID { get; set; }

        // Address info
        public string Address { get; set; }

        public string County { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        // Contact info.
        public string HomePhone { get; set; }

        public string WorkPhone { get; set; }

        public string CellPhone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }


        public override bool Equals(object obj)
        {
            Contact comp;
            if ((comp = obj as Contact) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
