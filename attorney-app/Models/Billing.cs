﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class Billing
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public DateTime CaseSettleDate { get; set; }

        public double LiensAmount { get; set; }

        public double AttorneyCompensationAmount { get; set; }

        public double ClientCompensationAmount { get; set; }

        public double TotalBill { get; set; }

        public bool IsPaid { get; set; }


        public override bool Equals(object obj)
        {
            Billing comp;
            if ((comp = obj as Billing) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
