﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class TemplateField
    {
        public string Name { get; set; }


        public override bool Equals(object obj)
        {
            TemplateField comp;
            if ((comp = obj as TemplateField) != null)
            {
                return Name == comp.Name;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
