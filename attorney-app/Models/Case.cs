﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Models
{
    public class Case
    {
        public string ID { get; set; } // AKA case number

        public string Name { get; set; }

        public bool IsDone { get; set; }

        public CaseType Type { get; set; }

        public DateTime AddedTime { get; set; }

        public DateTime RetainerDate { get; set; }

        public DateTime AccidentDate { get; set; }

        public DateTime CaseMaxDate { get; set; } // AKA statue of limitation.

        public DateTime ApplicationDeadlineDate { get; set; }

        public string TrialVenue { get; set; }

        public string Description { get; set; }

        public string LiabilityLimits { get; set; }

        
        public override bool Equals(object obj)
        {
            Case comp;
            if ((comp = obj as Case) != null)
            {
                return ID == comp.ID;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
