﻿using attorney_app.Commons;
using attorney_app.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for BillingsPage.xaml
    /// </summary>
    public partial class BillingsPage : Page
    {
        // View models.
        class BillingViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            Billing model;

            public Billing Model
            {
                get
                {
                    return model;
                }
                set
                {
                    model = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TotalBillString"));
                }
            }

            public string Name
            {
                get
                {
                    return Model.Name;
                }
            }

            public string TotalBillString
            {
                get
                {
                    return Model.TotalBill.ToString() + " USD";
                }
            }


            public BillingViewModel(Billing model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                BillingViewModel comp;
                if ((comp = obj as BillingViewModel) != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        ObservableCollection<BillingViewModel> m_unpaidBillings;
        ObservableCollection<BillingViewModel> m_paidBillings;
        BillingDetailsPage m_billingDetailsPage;
        bool m_isBackPermitted = true;


        public BillingsPage(MainWindow mainwindow, SQLiteConnection dbConn)
        {
            InitializeComponent();

            m_dbConn = dbConn;

            m_unpaidBillings = new ObservableCollection<BillingViewModel>();
            lstBillings_unpaid.ItemsSource = m_unpaidBillings;

            m_paidBillings = new ObservableCollection<BillingViewModel>();
            lstBillings_paid.ItemsSource = m_paidBillings;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (content) =>
            {
                if (content == this && m_isBackPermitted)
                {
                    // Going back.
                    m_mainwindow.GoBack();
                }
            };
            m_mainwindow.PageLoaded += async (content) =>
            {
                if (content == this)
                {
                    m_isBackPermitted = false;

                    try
                    {
                        // Loading billings.
                        await LoadBillings();
                    }
                    catch (Exception ex)
                    {
                        m_mainwindow.ShowMessage("Error", "Unable to load billings.\nDetails: " + ex.Message);
                    }
                    finally
                    {
                        m_isBackPermitted = true;
                    }
                }
            };
        }

        async Task LoadBillings()
        {
            m_paidBillings.Clear();
            m_unpaidBillings.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT * FROM billings";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        Billing billing = new Billing();
                        billing.ID = reader.GetString(0);
                        billing.Name = reader.GetString(1);
                        billing.CaseSettleDate = reader.GetDateTime(2);
                        billing.LiensAmount = reader.GetDouble(3);
                        billing.AttorneyCompensationAmount = reader.GetDouble(4);
                        billing.ClientCompensationAmount = reader.GetDouble(5);
                        billing.TotalBill = reader.GetDouble(6);
                        billing.IsPaid = reader.GetInt32(7) == 1;

                        if (billing.IsPaid)
                        {
                            m_paidBillings.Add(new BillingViewModel(billing));
                        }
                        else
                        {
                            m_unpaidBillings.Add(new BillingViewModel(billing));
                        }
                    }
                }
            }
        }

        void InitBillingDetailsPage()
        {
            if (m_billingDetailsPage != null)
            {
                throw new Exception("Billing details page was initialized");
            }

            m_billingDetailsPage = new BillingDetailsPage(m_mainwindow, m_dbConn);
            m_billingDetailsPage.BillingAdded += (billing) =>
            {
                if (billing.IsPaid)
                {
                    m_paidBillings.Add(new BillingViewModel(billing));
                }
                else
                {
                    m_unpaidBillings.Add(new BillingViewModel(billing));
                }
            };
            m_billingDetailsPage.BillingChanged += async (billing) =>
            {
                m_isBackPermitted = false;

                try
                {
                    await LoadBillings();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to load billings.\nDetails: " + ex.Message);
                }
                finally
                {
                    m_isBackPermitted = true;
                }
            };
        }

        #region UNPAID

        private void btnAddBilling_unpaid_Click(object sender, RoutedEventArgs e)
        {
            if (m_billingDetailsPage == null)
            {
                InitBillingDetailsPage();
            }

            // Loading the view.
            m_billingDetailsPage.Load(null);

            // Navigating.
            m_mainwindow.Navigate(m_billingDetailsPage);
        }

        private async void btnDeleteBilling_unpaid_Click(object sender, RoutedEventArgs e)
        {
            BillingViewModel selitem;
            if ((selitem = lstBillings_unpaid.SelectedItem as BillingViewModel) != null)
            {
                btnDeleteBilling_unpaid.IsEnabled = false;

                try
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        // Deleting the billing documents.
                        cmd.CommandText = "SELECT file_id FROM billing_file_links WHERE billing_id = @billingId";
                        cmd.Parameters.AddWithValue("billingId", selitem.Model.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                string fileId = reader.GetString(0);

                                // Deleting the file (will also delete from the link table).
                                await DatabaseAccess.DeleteFile(m_dbConn, fileId);
                            }
                        }

                        cmd.Reset();
                        cmd.CommandText = "DELETE FROM billings WHERE id = @billingId";
                        cmd.Parameters.AddWithValue("billingId", selitem.Model.ID);

                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Removing from the list.
                    m_unpaidBillings.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected billing.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteBilling_unpaid.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a billing from the list first!");
            }
        }

        private void btnMoreInfo_unpaid_Click(object sender, RoutedEventArgs e)
        {
            BillingViewModel selitem;
            if ((selitem = lstBillings_unpaid.SelectedItem as BillingViewModel) != null)
            {
                if (m_billingDetailsPage == null)
                {
                    InitBillingDetailsPage();
                }

                // Loading the view.
                m_billingDetailsPage.Load(selitem.Model);

                // Navigating.
                m_mainwindow.Navigate(m_billingDetailsPage);
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a billing from the list first!");
            }
        }

        #endregion

        #region PAID

        private async void btnDeleteBilling_paid_Click(object sender, RoutedEventArgs e)
        {
            BillingViewModel selitem;
            if ((selitem = lstBillings_paid.SelectedItem as BillingViewModel) != null)
            {
                btnDeleteBilling_paid.IsEnabled = false;

                try
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        // Deleting the billing documents.
                        cmd.CommandText = "SELECT file_id FROM billing_file_links WHERE billing_id = @billingId";
                        cmd.Parameters.AddWithValue("billingId", selitem.Model.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                string fileId = reader.GetString(0);

                                // Deleting the file (will also delete from the link table).
                                await DatabaseAccess.DeleteFile(m_dbConn, fileId);
                            }
                        }

                        cmd.Reset();
                        cmd.CommandText = "DELETE FROM billings WHERE id = @billingId";
                        cmd.Parameters.AddWithValue("billingId", selitem.Model.ID);

                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Removing from the list.
                    m_paidBillings.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected billing.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteBilling_paid.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a billing from the list first!");
            }
        }

        private void btnMoreInfo_paid_Click(object sender, RoutedEventArgs e)
        {
            BillingViewModel selitem;
            if ((selitem = lstBillings_paid.SelectedItem as BillingViewModel) != null)
            {
                if (m_billingDetailsPage == null)
                {
                    InitBillingDetailsPage();
                }

                // Loading the view.
                m_billingDetailsPage.Load(selitem.Model);

                // Navigating.
                m_mainwindow.Navigate(m_billingDetailsPage);
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a billing from the list first!");
            }
        }

        #endregion
    }
}
