﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        const string WINDOW_TITLE = "Attorney's App";

        public delegate void PageLoadedHandler(Page content);
        public delegate void BackRequestedHandler(Page content);

        public event PageLoadedHandler PageLoaded;
        public event BackRequestedHandler BackRequested;


        public MainWindow()
        {
            InitializeComponent();

            // Navigating to the mainpage.
            MainPage mainPage = new MainPage(this);
            Navigate(mainPage);
        }

        public void Navigate(Page page)
        {
            frmMain.Navigate(page);
        }

        public void GoBack()
        {
            if (frmMain.CanGoBack)
            {
                frmMain.GoBack();
            }
        }

        public async void ShowMessage(string title, string message)
        {
            await this.ShowMessageAsync(title, message);
        }

        public async Task<bool> ShowPrompt(string title, string content)
        {
            MessageDialogResult result = await this.ShowMessageAsync(title, content, MessageDialogStyle.AffirmativeAndNegative);
            return result == MessageDialogResult.Affirmative;
        }

        void ShowBackButton()
        {
            LeftWindowCommands.Width = btnBack.Width;
        }

        void HideBackButton()
        {
            LeftWindowCommands.Width = 0;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            BackRequested?.Invoke(frmMain.Content as Page);
        }

        private void frmMain_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            if (frmMain.CanGoBack)
            {
                ShowBackButton();
            }
            else
            {
                HideBackButton();
            }

            // Notifying the pages.
            PageLoaded?.Invoke(frmMain.Content as Page);

            // Changing the window title.
            this.Title = string.Format("{0} - {1}", (frmMain.Content as Page).Title, WINDOW_TITLE);
        }
    }
}
