﻿using attorney_app.Commons;
using attorney_app.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for TasksPage.xaml
    /// </summary>
    public partial class TasksPage : Page
    {
        // Required view models.
        class AttorneyTaskViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            AttorneyTask model;

            public AttorneyTask Model
            {
                get
                {
                    return model;
                }
                set
                {
                    model = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ContactName"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Status"));
                }
            }

            public string ContactName
            {
                get
                {
                    return Model.Contact == null ? "Not specified" : Model.Contact.Name;
                }
            }

            public string Status
            {
                get
                {
                    if (Model.IsDone)
                    {
                        return "Done";
                    }
                    else
                    {
                        if (model.TimeEnd < DateTime.Now)
                        {
                            return "Probably missed";
                        }
                        else
                        {
                            if (model.TimeStart.Date == DateTime.Now.Date)
                            {
                                return string.Format("Today at {0} - {1}", Model.TimeStart.ToString("HH:mm"), Model.TimeEnd.ToString("HH:mm"));
                            }
                            else
                            {
                                return string.Format("At {0}", Model.TimeStart.ToLongDateString());
                            }
                        }
                    }
                }
            }

            public string Details
            {
                get
                {
                    if (Model.Details.Length > 30)
                    {
                        return string.Format("{0}...", Model.Details.Trim().Substring(0, 30).Replace('\n', ' ').Replace('\r', ' '));
                    }
                    else
                    {
                        return Model.Details;
                    }
                }
            }


            public AttorneyTaskViewModel(AttorneyTask model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                AttorneyTaskViewModel comp;
                if ((comp = obj as AttorneyTaskViewModel) != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        ObservableCollection<AttorneyTaskViewModel> m_tasks;
        ObservableCollection<Contact> m_contacts;
        AttorneyTaskViewModel m_taskBeingEdited;
        bool m_isBackPermitted = true;


        public TasksPage(MainWindow mainwindow, SQLiteConnection dbConn)
        {
            InitializeComponent();

            m_dbConn = dbConn;

            m_tasks = new ObservableCollection<AttorneyTaskViewModel>();
            lstTasks.ItemsSource = m_tasks;

            m_contacts = new ObservableCollection<Contact>();
            cmbContacts_gridTask.ItemsSource = m_contacts;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (content) =>
            {
                if (content == this && m_isBackPermitted)
                {
                    if (gridTask.Visibility == Visibility.Visible)
                    {
                        // Hiding the grid.
                        btnCancel_gridTask_Click(null, null);
                    }
                    else
                    {
                        // Going back.
                        m_mainwindow.GoBack();
                    }
                }
            };
            m_mainwindow.PageLoaded += async (content) =>
            {
                if (content == this)
                {
                    m_isBackPermitted = false;

                    try
                    {
                        // Loading the cmbContacts_gridTask.
                        await LoadContactsComboBox();

                        // Loading the tasks.
                        await LoadTasks();
                    }
                    catch (Exception ex)
                    {
                        m_mainwindow.ShowMessage("Error", "Unable to load tasks.\nDetails: " + ex.Message);
                    }
                    finally
                    {
                        m_isBackPermitted = true;
                    }
                }
            };
        }

        async Task LoadContactsComboBox()
        {
            m_contacts.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT contacts.id, contacts.name, contacts.group_id, contact_groups.value FROM contacts, contact_groups ";
                cmd.CommandText += "WHERE contacts.group_id = contact_groups.id ORDER BY contacts.name ASC";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        Contact contact = new Contact();
                        contact.ID = reader.GetString(0);
                        contact.Name = reader.GetString(1);

                        ContactGroup group = new ContactGroup();
                        group.ID = reader.GetInt32(2);
                        group.Name = reader.GetString(3);
                        contact.Group = group;

                        m_contacts.Add(contact);
                    }
                }
            }
        }

        async Task LoadTasks()
        {
            Contact GetContactInfo(string id)
            {
                for (int i = 0; i < m_contacts.Count; ++i)
                {
                    if (m_contacts[i].ID == id)
                    {
                        return m_contacts[i];
                    }
                }

                return null;
            }

            m_tasks.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT * FROM tasks WHERE is_done = 0 ORDER BY time_start ASC";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        AttorneyTask task = new AttorneyTask();
                        task.ID = reader.GetString(0);
                        task.Details = await reader.IsDBNullAsync(1) ? string.Empty : reader.GetString(1);

                        if (!await reader.IsDBNullAsync(2))
                        {
                            // Getting the contact info.
                            task.Contact = GetContactInfo(reader.GetString(2));
                        }

                        task.TimeStart = reader.GetDateTime(3);
                        task.TimeEnd = reader.GetDateTime(4);
                        task.IsDone = reader.GetInt32(5) == 1;

                        // Adding to the list.
                        m_tasks.Add(new AttorneyTaskViewModel(task));
                    }
                }
            }
        }

        private void btnAddTask_Click(object sender, RoutedEventArgs e)
        {
            gridTask.Visibility = Visibility.Visible;
        }

        private async void btnDeleteTask_Click(object sender, RoutedEventArgs e)
        {
            AttorneyTaskViewModel selitem;
            if ((selitem = lstTasks.SelectedItem as AttorneyTaskViewModel) != null)
            {
                btnDeleteTask.IsEnabled = false;

                try
                {
                    // Deleting from the database.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "DELETE FROM tasks WHERE id = @taskId";
                        cmd.Parameters.AddWithValue("taskId", selitem.Model.ID);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Removing from the list.
                    m_tasks.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteTask.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a task from the list first!");
            }
        }

        private void btnMoreInfo_Click(object sender, RoutedEventArgs e)
        {
            AttorneyTaskViewModel selitem;
            if ((selitem = lstTasks.SelectedItem as AttorneyTaskViewModel) != null)
            {
                m_taskBeingEdited = selitem;

                // Filling the user inputs.
                cmbContacts_gridTask.SelectedItem = selitem.Model.Contact;
                dtDate_gridTask.SelectedDate = selitem.Model.TimeStart.Date;
                timeStart_gridTask.SelectedTime = selitem.Model.TimeStart.TimeOfDay;
                timeEnd_gridTask.SelectedTime = selitem.Model.TimeEnd.TimeOfDay;
                chkIsDone_gridTask.IsChecked = selitem.Model.IsDone;
                txtDetails_gridTask.Text = selitem.Model.Details;

                // Showing the grid.
                gridTask.Visibility = Visibility.Visible;
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a task from the list first!");
            }
        }

        private async void btnOK_gridTask_Click(object sender, RoutedEventArgs e)
        {
            m_isBackPermitted = false;
            btnOK_gridTask.IsEnabled = false;
            btnCancel_gridTask.IsEnabled = false;

            try
            {
                // Checking the user inputs.
                if (!dtDate_gridTask.SelectedDate.HasValue)
                {
                    throw new Exception("Date is undefined");
                }

                if (!timeStart_gridTask.SelectedTime.HasValue)
                {
                    throw new Exception("Start time is undefined");
                }

                if (!timeEnd_gridTask.SelectedTime.HasValue)
                {
                    throw new Exception("End time is undefined");
                }

                if (string.IsNullOrEmpty(txtDetails_gridTask.Text))
                {
                    throw new Exception("Details is null");
                }

                if (DateTime.Now.Date > dtDate_gridTask.SelectedDate.Value && DateTime.Now.TimeOfDay > timeStart_gridTask.SelectedTime.Value)
                {
                    throw new Exception("Cannot create task before current time");
                }

                if (timeStart_gridTask.SelectedTime.Value > timeEnd_gridTask.SelectedTime.Value)
                {
                    throw new Exception("Start time is greater than the end time");
                }

                // Creating new instance.
                AttorneyTask task = new AttorneyTask();

                if (m_taskBeingEdited == null)
                {
                    // Generating new ID.
                    task.ID = await Randomizer.RandomStringAsync(32);
                }
                else
                {
                    task.ID = m_taskBeingEdited.Model.ID;
                }

                task.Contact = cmbContacts_gridTask.SelectedItem as Contact;

                DateTime date = dtDate_gridTask.SelectedDate.Value;
                TimeSpan start = timeStart_gridTask.SelectedTime.Value;
                TimeSpan end = timeEnd_gridTask.SelectedTime.Value;

                task.TimeStart = date.Add(start);
                task.TimeEnd = date.Add(end);

                task.IsDone = chkIsDone_gridTask.IsChecked.Value;
                task.Details = txtDetails_gridTask.Text;

                // Saving to the database.
                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                {
                    if (m_taskBeingEdited == null)
                    {
                        // Create new.
                        cmd.CommandText = "INSERT INTO tasks(id, contact_id, time_start, time_end, is_done, details) VALUES(";
                        cmd.CommandText += "@taskId, @contactId, @timeStart, @timeEnd, @isDoneInt, @details)";
                    }
                    else
                    {
                        // Update existing.
                        cmd.CommandText = "UPDATE tasks SET contact_id = @contactId, time_start = @timeStart, time_end = @timeEnd, is_done = @isDoneInt, details = @details ";
                        cmd.CommandText += "WHERE id = @taskId";
                    }

                    cmd.Parameters.AddWithValue("taskId", task.ID);
                    cmd.Parameters.AddWithValue("contactId", task.Contact == null ? null : task.Contact.ID);
                    cmd.Parameters.AddWithValue("timeStart", task.TimeStart);
                    cmd.Parameters.AddWithValue("timeEnd", task.TimeEnd);
                    cmd.Parameters.AddWithValue("isDoneInt", task.IsDone ? 1 : 0);
                    cmd.Parameters.AddWithValue("details", task.Details);
                    await cmd.ExecuteNonQueryAsync();
                }

                if (m_taskBeingEdited == null)
                {
                    // Adding to the list.
                    m_tasks.Add(new AttorneyTaskViewModel(task));
                }
                else
                {
                    // Updating the view.
                    m_taskBeingEdited.Model = task;
                }

                // Done.
                btnCancel_gridTask_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to apply changes.\nDetails: " + ex.Message);
            }
            finally
            {
                m_isBackPermitted = true;
                btnOK_gridTask.IsEnabled = true;
                btnCancel_gridTask.IsEnabled = true;
            }
        }

        private void btnCancel_gridTask_Click(object sender, RoutedEventArgs e)
        {
            m_taskBeingEdited = null;

            // Clearing the user inputs.
            cmbContacts_gridTask.SelectedItem = null;
            dtDate_gridTask.SelectedDate = null;
            timeStart_gridTask.SelectedTime = null;
            timeEnd_gridTask.SelectedTime = null;
            chkIsDone_gridTask.IsChecked = false;
            txtDetails_gridTask.Clear();

            // Hiding the grid.
            gridTask.Visibility = Visibility.Hidden;
        }
    }
}
