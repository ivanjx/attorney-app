﻿using attorney_app.Commons;
using attorney_app.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for NewDocumentPage.xaml
    /// </summary>
    public partial class NewDocumentPage : Page
    {
        class TemplateFieldViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            string m_name;
            string m_value;

            public string Name
            {
                get
                {
                    return m_name;
                }
                set
                {
                    m_name = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
                }
            }

            public string Value
            {
                get
                {
                    return m_value;
                }
                set
                {
                    m_value = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Value"));
                }
            }
        }

        // Page definition.
        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        ObservableCollection<TemplateFieldViewModel> m_fields;
        Template m_usedTemplate;
        bool m_isBackPermitted = true;


        public NewDocumentPage(MainWindow mainwindow, SQLiteConnection dbConn)
        {
            InitializeComponent();

            m_dbConn = dbConn;

            m_fields = new ObservableCollection<TemplateFieldViewModel>();
            lstFields.ItemsSource = m_fields;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (content) =>
            {
                if (content == this && m_isBackPermitted)
                {
                    // Clearing user inputs.
                    ClearInputs();

                    // Going back.
                    m_mainwindow.GoBack();
                }
            };
        }

        public async void Load(Template template)
        {
            ClearInputs();
            m_isBackPermitted = false;

            try
            {
                if (template == null)
                {
                    throw new Exception("Template is null");
                }

                m_usedTemplate = template;

                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                {
                    cmd.CommandText = "SELECT field_name FROM template_fields WHERE template_id = @templateId";
                    cmd.Parameters.AddWithValue("templateId", template.ID);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            TemplateFieldViewModel vm = new TemplateFieldViewModel();
                            vm.Name = reader.GetString(0);
                            vm.Value = string.Empty;

                            m_fields.Add(vm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load fields.\nDetails: " + ex.Message);
            }
            finally
            {
                m_isBackPermitted = true;
            }
        }

        void ClearInputs()
        {
            m_fields.Clear();
            m_usedTemplate = null;
        }

        private async void btnDone_Click(object sender, RoutedEventArgs e)
        {
            pringLoading.IsActive = true;
            btnDone.IsEnabled = false;
            await Task.Delay(1000);

            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Title = "Select save location...";
                sfd.FileName = "New Document.docx";
                sfd.DefaultExt = ".docx";

                bool? result = sfd.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    string outputPath = sfd.FileName;
                    string outputTemplate = outputPath + ".dotx";

                    // Copying the file.
                    using (Stream inStream = DatabaseAccess.GetFileStream(m_usedTemplate.FileID, FileMode.Open))
                    using (Stream outStream = File.Open(outputTemplate, FileMode.Create))
                    {
                        await inStream.CopyToAsync(outStream);
                    }

                    await Task.Run(() =>
                    {
                        // Launching the app.
                        object targetPath = outputPath;
                        object visible = false;
                        Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
                        Microsoft.Office.Interop.Word.Document doc = app.Documents.Add(outputTemplate);

                        // Replacing the bookmarks.
                        foreach (TemplateFieldViewModel fieldVM in m_fields)
                        {
                            if (doc.Bookmarks.Exists(fieldVM.Name))
                            {
                                doc.Bookmarks[fieldVM.Name].Select();
                                app.Selection.TypeText(fieldVM.Value);
                            }
                        }

                        // Done.
                        doc.SaveAs(FileName: outputPath);
                        app.Quit();
                    });

                    await Task.Delay(5000);

                    // Deleting the temp file.
                    File.Delete(outputTemplate);

                    // Done.
                    m_mainwindow.ShowMessage("Info", "Successfully created new document.");
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to create new document.\nDetails: " + ex.Message);
            }
            finally
            {
                pringLoading.IsActive = false;
                btnDone.IsEnabled = true;
            }
        }
    }
}
