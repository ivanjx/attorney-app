﻿using attorney_app.Commons;
using attorney_app.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for CaseDetailsPage.xaml
    /// </summary>
    public partial class CaseDetailsPage : Page
    {
        // View models.
        class CaseDocumentViewModel
        {
            public CaseDocument Model { get; private set; }

            public string OriginalName
            {
                get
                {
                    return Model.OriginalName;
                }
            }

            public string Description
            {
                get
                {
                    return Model.Description;
                }
            }

            public string SizeString
            {
                get
                {
                    return string.Format("{0} KB", (Model.Size / 1024.0).ToString("F1"));
                }
            }


            public CaseDocumentViewModel(CaseDocument model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                CaseDocumentViewModel comp;
                if ((comp = obj as CaseDocumentViewModel) != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        class CaseNoteViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            CaseNote model;

            public CaseNote Model
            {
                get
                {
                    return model;
                }
                set
                {
                    model = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Body"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TimeCreatedString"));
                }
            }

            public string Body
            {
                get
                {
                    string ret = Model.Body;
                    ret = ret.Replace('\n', ' ').Replace('\r', ' ');

                    if (ret.Length > 30)
                    {
                        return string.Format("{0}...", ret.Substring(0, 30));
                    }
                    else
                    {
                        return ret;
                    }
                }
            }

            public string TimeCreatedString
            {
                get
                {
                    return Model.TimeCreated.ToString();
                }
            }


            public CaseNoteViewModel(CaseNote model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                CaseNoteViewModel comp;
                if ((comp = obj as CaseNoteViewModel) != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        public delegate void CaseAddedHandler(Case addedCase);
        public delegate void CaseChangedHandler(Case changedCase);

        public event CaseAddedHandler CaseAdded;
        public event CaseChangedHandler CaseChanged;

        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        Case m_caseBeingEdited;
        CaseNoteViewModel m_caseNoteBeingEdited;
        ObservableCollection<Contact> m_allContacts;
        ObservableCollection<Contact> m_connectedContacts;
        ObservableCollection<CaseDocumentViewModel> m_caseDocuments;
        ObservableCollection<CaseNoteViewModel> m_caseNotes;
        bool m_isBackPermitted = true;


        public CaseDetailsPage(MainWindow mainwindow, SQLiteConnection dbConn)
        {
            InitializeComponent();

            m_dbConn = dbConn;

            m_allContacts = new ObservableCollection<Contact>();
            cmbContact_gridConnectContact.ItemsSource = m_allContacts;

            m_connectedContacts = new ObservableCollection<Contact>();
            lstContacts.ItemsSource = m_connectedContacts;

            m_caseDocuments = new ObservableCollection<CaseDocumentViewModel>();
            lstDocs.ItemsSource = m_caseDocuments;

            m_caseNotes = new ObservableCollection<CaseNoteViewModel>();
            lstNotes.ItemsSource = m_caseNotes;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (content) =>
            {
                if (content == this && m_isBackPermitted)
                {
                    // Throwing away the case instance.
                    m_caseBeingEdited = null;

                    // Clearing user inputs.
                    ClearInputs();

                    // Going back.
                    m_mainwindow.GoBack();
                }
            };
        }

        public async void Load(Case caseBeingEdited, ObservableCollection<CaseType> caseTypes)
        {
            m_isBackPermitted = false;
            cmbType.ItemsSource = caseTypes;

            try
            {
                // Reloading contacts list.
                await LoadAllContacts();

                // Filling the user inputs.
                if (caseBeingEdited != null)
                {
                    m_caseBeingEdited = caseBeingEdited;

                    txtID.IsEnabled = false; // ID cant be changed.
                    txtID.Text = caseBeingEdited.ID;
                    txtName.Text = caseBeingEdited.Name;
                    cmbType.SelectedItem = caseBeingEdited.Type;

                    if (caseBeingEdited.RetainerDate != default(DateTime))
                    {
                        dtRetainer.SelectedDate = caseBeingEdited.RetainerDate;
                    }

                    if (caseBeingEdited.AccidentDate != default(DateTime))
                    {
                        dtAccident.SelectedDate = caseBeingEdited.AccidentDate;
                    }

                    if (caseBeingEdited.ApplicationDeadlineDate != default(DateTime))
                    {
                        dtApplicationDeadline.SelectedDate = caseBeingEdited.ApplicationDeadlineDate;
                    }

                    if (caseBeingEdited.CaseMaxDate != default(DateTime))
                    {
                        dtMaxDate.SelectedDate = caseBeingEdited.CaseMaxDate;
                    }

                    txtDescription.Text = caseBeingEdited.Description;
                    txtTrialVenue.Text = caseBeingEdited.TrialVenue;
                    txtLiabilityLimits.Text = caseBeingEdited.LiabilityLimits;
                    chkIsDone.IsChecked = caseBeingEdited.IsDone;

                    // Loading connected contacts.
                    await LoadConnectedContacts();

                    // Loading documents.
                    await LoadCaseDocs();

                    // Loading notes.
                    await LoadCaseNotes();
                }
                else
                {
                    txtID.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load case.\nDetails: " + ex.Message);

                // Going back.
                ClearInputs();
                m_mainwindow.GoBack();
            }
            finally
            {
                m_isBackPermitted = true;
            }
        }

        async Task LoadAllContacts()
        {
            m_allContacts.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT contacts.id, contacts.name, contacts.county, contacts.state, contacts.group_id, contact_groups.value ";
                cmd.CommandText += "FROM contacts, contact_groups ";
                cmd.CommandText += "WHERE contacts.group_id = contact_groups.id";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        Contact contact = new Contact();
                        contact.ID = reader.GetString(0);
                        contact.Name = reader.GetString(1);
                        contact.County = reader.GetString(2);
                        contact.State = reader.GetString(3);

                        ContactGroup group = new ContactGroup();
                        group.ID = reader.GetInt32(4);
                        group.Name = reader.GetString(5);
                        contact.Group = group;

                        m_allContacts.Add(contact);
                    }
                }
            }
        }

        async Task LoadConnectedContacts()
        {
            m_connectedContacts.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT contact_id FROM case_contact_links WHERE case_id = @caseId";
                cmd.Parameters.AddWithValue("caseId", m_caseBeingEdited.ID);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        string contactId = reader.GetString(0);

                        // Finding macthing contact id.
                        for (int i = 0; i < m_allContacts.Count; ++i)
                        {
                            if (m_allContacts[i].ID == contactId)
                            {
                                m_connectedContacts.Add(m_allContacts[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }

        async Task LoadCaseDocs()
        {
            m_caseDocuments.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT files.id, files.original_name, case_file_links.description ";
                cmd.CommandText += "FROM case_file_links, files ";
                cmd.CommandText += "WHERE case_file_links.case_id = @caseId AND case_file_links.file_id = files.id";
                cmd.Parameters.AddWithValue("caseId", m_caseBeingEdited.ID);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        CaseDocument doc = new CaseDocument();
                        doc.FileID = reader.GetString(0);
                        doc.OriginalName = reader.GetString(1);
                        doc.Description = reader.GetString(2);

                        // Getting the file size.
                        using (Stream fs = DatabaseAccess.GetFileStream(doc.FileID, FileMode.Open))
                        {
                            doc.Size = fs.Length;
                        }

                        m_caseDocuments.Add(new CaseDocumentViewModel(doc));
                    }
                }
            }
        }

        async Task LoadCaseNotes()
        {
            m_caseNotes.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT notes.id, notes.body, notes.time_created ";
                cmd.CommandText += "FROM notes, case_note_links ";
                cmd.CommandText += "WHERE case_note_links.case_id = @caseId AND case_note_links.note_id = notes.id ";
                cmd.CommandText += "ORDER BY notes.time_created ASC";
                cmd.Parameters.AddWithValue("caseId", m_caseBeingEdited.ID);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        CaseNote note = new CaseNote();
                        note.ID = reader.GetString(0);
                        note.Body = reader.GetString(1);
                        note.TimeCreated = reader.GetDateTime(2);

                        m_caseNotes.Add(new CaseNoteViewModel(note));
                    }
                }
            }
        }

        void ClearInputs()
        {
            m_caseBeingEdited = null;

            // Clearing the details tab.
            txtID.Clear();
            txtName.Clear();
            cmbType.SelectedItem = null;
            dtRetainer.SelectedDate = null;
            dtAccident.SelectedDate = null;
            dtApplicationDeadline.SelectedDate = null;
            dtMaxDate.SelectedDate = null;
            txtDescription.Clear();
            txtTrialVenue.Clear();
            txtLiabilityLimits.Clear();
            chkIsDone.IsChecked = false;

            // Clearing the contacts tab.
            m_connectedContacts.Clear();
            btnCancel_gridConnectContact_Click(null, null);

            // Clearing the docs tab.
            m_caseDocuments.Clear();
            btnCancel_gridDoc_Click(null, null);

            // Clearing the notes tab.
            m_caseNotes.Clear();
            btnCancel_gridNote_Click(null, null);
        }

        private async void btnSave_Click(object sender, RoutedEventArgs e)
        {
            // This function only saves the details tab.
            btnSave.IsEnabled = false;
            m_isBackPermitted = false;

            try
            {
                // Checking the user inputs.
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    throw new Exception("ID is null");
                }

                if (string.IsNullOrEmpty(txtName.Text))
                {
                    throw new Exception("Name is null");
                }

                if (cmbType.SelectedItem as CaseType == null)
                {
                    throw new Exception("Case type was not selected");
                }

                // Creating new instance.
                Case acase = new Case();
                acase.ID = txtID.Text;
                acase.Name = txtName.Text;
                acase.Type = cmbType.SelectedItem as CaseType;
                acase.RetainerDate = dtRetainer.SelectedDate.HasValue ? dtRetainer.SelectedDate.Value : default(DateTime);
                acase.AccidentDate = dtAccident.SelectedDate.HasValue ? dtAccident.SelectedDate.Value : default(DateTime);
                acase.ApplicationDeadlineDate = dtApplicationDeadline.SelectedDate.HasValue ? dtApplicationDeadline.SelectedDate.Value : default(DateTime);
                acase.CaseMaxDate = dtMaxDate.SelectedDate.HasValue ? dtMaxDate.SelectedDate.Value : default(DateTime);
                acase.Description = txtDescription.Text;
                acase.TrialVenue = txtTrialVenue.Text;
                acase.LiabilityLimits = txtLiabilityLimits.Text;
                acase.IsDone = chkIsDone.IsChecked.Value;

                if (m_caseBeingEdited == null)
                {
                    acase.AddedTime = DateTime.Now;
                }
                else
                {
                    acase.AddedTime = m_caseBeingEdited.AddedTime;
                }

                // Creating transaction.
                using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                {
                    // Saving changes to the database.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        if (m_caseBeingEdited == null)
                        {
                            // Create new.
                            cmd.CommandText = "INSERT INTO cases(id, name, type_id, date_retainer, date_accident, date_max, date_app_deadline, description, trial_venue, liability_limits, is_done, time_added) ";
                            cmd.CommandText += "VALUES(@caseId, @name, @typeId, @retainerDate, @accidentDate, @maxDate, @appDeadlineDate, @description, @trialVenue, @liabilityLimits, @isDoneInt, @addedTime)";
                        }
                        else
                        {
                            // Update existing.
                            cmd.CommandText = "UPDATE cases SET name = @name, type_id = @typeId, date_retainer = @retainerDate, date_accident = @accidentDate, date_max = @maxDate, date_app_deadline = @appDeadlineDate, ";
                            cmd.CommandText += "description = @description, trial_venue = @trialVenue, liability_limits = @liabilityLimits, is_done = @isDoneInt WHERE id = @caseId";
                        }

                        cmd.Parameters.AddWithValue("caseId", acase.ID);
                        cmd.Parameters.AddWithValue("name", acase.Name);
                        cmd.Parameters.AddWithValue("typeId", acase.Type.ID);
                        cmd.Parameters.AddWithValue("retainerDate", acase.RetainerDate);
                        cmd.Parameters.AddWithValue("accidentDate", acase.AccidentDate);
                        cmd.Parameters.AddWithValue("maxDate", acase.CaseMaxDate);
                        cmd.Parameters.AddWithValue("appDeadlineDate", acase.ApplicationDeadlineDate);
                        cmd.Parameters.AddWithValue("description", acase.Description);
                        cmd.Parameters.AddWithValue("trialVenue", acase.TrialVenue);
                        cmd.Parameters.AddWithValue("liabilityLimits", acase.LiabilityLimits);
                        cmd.Parameters.AddWithValue("isDoneInt", acase.IsDone ? 1 : 0);
                        cmd.Parameters.AddWithValue("addedTime", acase.AddedTime);

                        await cmd.ExecuteNonQueryAsync();
                    }

                    if (m_caseBeingEdited == null)
                    {
                        // Saving the connected contacts.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            for (int i = 0; i < m_connectedContacts.Count; ++i)
                            {
                                // Saving to the case_contact_links table
                                cmd.CommandText = "INSERT INTO case_contact_links(case_id, contact_id) VALUES(@caseId, @contactId)";
                                cmd.Parameters.AddWithValue("caseId", acase.ID);
                                cmd.Parameters.AddWithValue("contactId", m_connectedContacts[i].ID);

                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();
                            }
                        }

                        // Saving the documents.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            for (int i = 0; i < m_caseDocuments.Count; ++i)
                            {
                                // Uploading.
                                string fileId = await DatabaseAccess.UploadFile(m_dbConn, m_caseDocuments[i].Model.OriginalPath, m_caseDocuments[i].Model.OriginalName);

                                // Saving to case_file_links table.
                                cmd.CommandText = "INSERT INTO case_file_links(case_id, file_id, description) VALUES(@caseId, @fileId, @description)";
                                cmd.Parameters.AddWithValue("caseId", acase.ID);
                                cmd.Parameters.AddWithValue("fileId", fileId);
                                cmd.Parameters.AddWithValue("description", m_caseDocuments[i].Model.Description);

                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();
                            }
                        }

                        // Saving notes.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            for (int i = 0; i < m_caseNotes.Count; ++i)
                            {
                                // Adding to notes table.
                                cmd.CommandText = "INSERT INTO notes(id, body, time_created) VALUES(@id, @body, @timeCreated)";
                                cmd.Parameters.AddWithValue("id", m_caseNotes[i].Model.ID);
                                cmd.Parameters.AddWithValue("body", m_caseNotes[i].Model.Body);
                                cmd.Parameters.AddWithValue("timeCreated", m_caseNotes[i].Model.TimeCreated);

                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();

                                // Adding to the link table.
                                cmd.CommandText = "INSERT INTO case_note_links(case_id, note_id) VALUES(@caseId, @noteId)";
                                cmd.Parameters.AddWithValue("caseId", acase.ID);
                                cmd.Parameters.AddWithValue("noteId", m_caseNotes[i].Model.ID);

                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();
                            }
                        }

                        // Reloading everything.
                        ClearInputs();
                        Load(acase, cmbType.ItemsSource as ObservableCollection<CaseType>);

                        // Telling the cases page.
                        CaseAdded?.Invoke(acase);
                    }
                    else
                    {
                        // Telling the cases page.
                        CaseChanged?.Invoke(acase);
                    }

                    // Commiting changes.
                    transaction.Commit();
                }

                // Done.
                m_mainwindow.ShowMessage("Info", "Changes have been saved.");
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave.IsEnabled = true;
                m_isBackPermitted = true;
            }
        }

        #region CONTACTS_TAB

        private void btnConnectContact_Click(object sender, RoutedEventArgs e)
        {
            gridConnectContact.Visibility = Visibility.Visible;
        }

        private async void btnRemoveContact_Click(object sender, RoutedEventArgs e)
        {
            Contact selitem;
            if ((selitem = lstContacts.SelectedItem as Contact) != null)
            {
                if (m_caseBeingEdited != null)
                {
                    btnRemoveContact.IsEnabled = false;

                    try
                    {
                        // Deleting from the link table.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "DELETE FROM case_contact_links WHERE case_id = @caseId AND contact_id = @contactId";
                            cmd.Parameters.AddWithValue("caseId", m_caseBeingEdited.ID);
                            cmd.Parameters.AddWithValue("contactId", selitem.ID);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        m_mainwindow.ShowMessage("Error", "Unable to remove selected contact.\nDetails: " + ex.Message);
                    }
                    finally
                    {
                        btnRemoveContact.IsEnabled = true;
                    }
                }

                // Removing from the list.
                m_connectedContacts.Remove(selitem);
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a contact from the list first!");
            }
        }

        private async void btnOK_gridConnectContact_Click(object sender, RoutedEventArgs e)
        {
            btnOK_gridConnectContact.IsEnabled = false;
            btnCancel_gridConnectContact.IsEnabled = false;

            try
            {
                Contact selContact = null;

                if ((selContact = cmbContact_gridConnectContact.SelectedItem as Contact) == null)
                {
                    throw new Exception("No contact selected");
                }

                if (m_caseBeingEdited != null)
                {
                    // Saving to the link table.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "INSERT INTO case_contact_links(case_id, contact_id) VALUES(@caseId, @contactId)";
                        cmd.Parameters.AddWithValue("caseId", m_caseBeingEdited.ID);
                        cmd.Parameters.AddWithValue("contactId", selContact.ID);

                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                // Adding to the list.
                m_connectedContacts.Add(selContact);

                // Done.
                btnCancel_gridConnectContact_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable add a contact.\nDetails: " + ex.Message);
            }
            finally
            {
                btnOK_gridConnectContact.IsEnabled = true;
                btnCancel_gridConnectContact.IsEnabled = true;
            }
        }

        private void btnCancel_gridConnectContact_Click(object sender, RoutedEventArgs e)
        {
            // Clearing user inputs.
            cmbContact_gridConnectContact.SelectedItem = null;

            // Hiding the grid.
            gridConnectContact.Visibility = Visibility.Hidden;
        }

        #endregion

        #region DOCS_TAB

        private void btnAddDoc_Click(object sender, RoutedEventArgs e)
        {
            gridDoc.Visibility = Visibility.Visible;
        }

        private async void btnDeleteDoc_Click(object sender, RoutedEventArgs e)
        {
            CaseDocumentViewModel selitem;
            if ((selitem = lstDocs.SelectedItem as CaseDocumentViewModel) != null)
            {
                if (m_caseBeingEdited != null)
                {
                    btnDeleteDoc.IsEnabled = false;

                    try
                    {
                        // Just delete the file (ON DELETE CASCADE).
                        await DatabaseAccess.DeleteFile(m_dbConn, selitem.Model.FileID);
                    }
                    catch (Exception ex)
                    {
                        m_mainwindow.ShowMessage("Error", "Unable to remove selected document.\nDetails: " + ex.Message);
                    }
                    finally
                    {
                        btnDeleteDoc.IsEnabled = true;
                    }
                }

                // Removing from the list.
                m_caseDocuments.Remove(selitem);
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a document from the list first!");
            }
        }

        private async void btnDownloadDoc_Click(object sender, RoutedEventArgs e)
        {
            CaseDocumentViewModel selitem;
            if ((selitem = lstDocs.SelectedItem as CaseDocumentViewModel) != null)
            {
                if (m_caseBeingEdited == null)
                {
                    // Opening the original file instead.
                    Process.Start(selitem.Model.OriginalPath);
                }
                else
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Title = "Select download file location...";
                    sfd.FileName = selitem.Model.OriginalName;
                    sfd.DefaultExt = Path.GetExtension(selitem.Model.OriginalName);

                    bool? result = sfd.ShowDialog();
                    if (result.HasValue && result.Value)
                    {
                        string targetPath = sfd.FileName;

                        using (Stream inStream = DatabaseAccess.GetFileStream(selitem.Model.FileID, FileMode.Open))
                        using (Stream outStream = File.Open(targetPath, FileMode.Create))
                        {
                            await inStream.CopyToAsync(outStream);
                        }

                        // Opening the file.
                        Process.Start(targetPath);
                    }
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a document from the list first!");
            }
        }

        private void btnBrowseFile_gridDoc_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select a file...";
            ofd.FileName = string.Empty;
            ofd.Multiselect = false;
            ofd.CheckFileExists = true;

            bool? result = ofd.ShowDialog();
            if (result.HasValue && result.Value)
            {
                txtFilePath_gridDoc.Text = ofd.FileName;
                txtFileName_gridDoc.Text = Path.GetFileName(ofd.FileName);
            }
        }

        private async void btnOK_gridDoc_Click(object sender, RoutedEventArgs e)
        {
            btnOK_gridDoc.IsEnabled = false;
            btnCancel_gridDoc.IsEnabled = false;

            try
            {
                // Checking the user inputs.
                if (string.IsNullOrEmpty(txtFilePath_gridDoc.Text))
                {
                    throw new Exception("No file selected");
                }

                if (string.IsNullOrEmpty(txtFileName_gridDoc.Text))
                {
                    throw new Exception("File name is null");
                }

                CaseDocument doc = new CaseDocument();
                doc.OriginalName = txtFileName_gridDoc.Text;
                doc.OriginalPath = txtFilePath_gridDoc.Text;
                doc.Description = txtDescription_gridDoc.Text;
                doc.Size = new FileInfo(doc.OriginalPath).Length;

                if (m_caseBeingEdited != null)
                {
                    // Uploading the file.
                    doc.FileID = await DatabaseAccess.UploadFile(m_dbConn, doc.OriginalPath, doc.OriginalName);

                    // Adding to the link table.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "INSERT INTO case_file_link(case_id, file_id, description) VALUES(@caseId, @fileId, @description)";
                        cmd.Parameters.AddWithValue("caseId", m_caseBeingEdited.ID);
                        cmd.Parameters.AddWithValue("fileId", doc.FileID);
                        cmd.Parameters.AddWithValue("description", doc.Description);

                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                // Adding to the list.
                m_caseDocuments.Add(new CaseDocumentViewModel(doc));

                // Done.
                btnCancel_gridDoc_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to add document.\nDetails: " + ex.Message);
            }
            finally
            {
                btnOK_gridDoc.IsEnabled = true;
                btnCancel_gridDoc.IsEnabled = true;
            }
        }

        private void btnCancel_gridDoc_Click(object sender, RoutedEventArgs e)
        {
            // Clearing user inputs.
            txtFilePath_gridDoc.Clear();
            txtFileName_gridDoc.Clear();
            txtDescription_gridDoc.Clear();

            // Hiding the grid.
            gridDoc.Visibility = Visibility.Hidden;
        }

        #endregion

        #region NOTES_TAB

        private void btnAddNote_Click(object sender, RoutedEventArgs e)
        {
            gridNote.Visibility = Visibility.Visible;
        }

        private async void btnDeleteNote_Click(object sender, RoutedEventArgs e)
        {
            CaseNoteViewModel selitem;
            if ((selitem = lstNotes.SelectedItem as CaseNoteViewModel) != null)
            {
                if (m_caseBeingEdited != null)
                {
                    btnDeleteNote.IsEnabled = false;

                    try
                    {
                        // Deleting from notes table.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "DELETE FROM notes WHERE id = @noteId";
                            cmd.Parameters.AddWithValue("noteId", selitem.Model.ID);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        m_mainwindow.ShowMessage("Error", "Unable to delete selected note.\nDetails: " + ex.Message);
                    }
                    finally
                    {
                        btnDeleteNote.IsEnabled = true;
                    }
                }

                // Removing from the list.
                m_caseNotes.Remove(selitem);
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a note from the list first!");
            }
        }

        private void btnEditNote_Click(object sender, RoutedEventArgs e)
        {
            CaseNoteViewModel selitem;
            if ((selitem = lstNotes.SelectedItem as CaseNoteViewModel) != null)
            {
                m_caseNoteBeingEdited = selitem;

                // Filling the user inputs.
                txtBody_gridNote.Text = m_caseNoteBeingEdited.Model.Body;

                // Showing the grid.
                gridNote.Visibility = Visibility.Visible;
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a note from the list first!");
            }
        }

        private async void btnOK_gridNote_Click(object sender, RoutedEventArgs e)
        {
            btnOK_gridNote.IsEnabled = false;
            btnCancel_gridNote.IsEnabled = false;

            try
            {
                // Checking the user inputs.
                if (string.IsNullOrEmpty(txtBody_gridNote.Text))
                {
                    throw new Exception("Note body is null");
                }

                CaseNote note = new CaseNote();

                if (m_caseNoteBeingEdited == null)
                {
                    // Generating new ID.
                    note.ID = await Randomizer.RandomStringAsync(32);
                    note.TimeCreated = DateTime.Now;
                }
                else
                {
                    note.ID = m_caseNoteBeingEdited.Model.ID;
                    note.TimeCreated = m_caseNoteBeingEdited.Model.TimeCreated;
                }

                note.Body = txtBody_gridNote.Text;

                if (m_caseBeingEdited != null)
                {
                    // Saving to notes table.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        if (m_caseNoteBeingEdited == null)
                        {
                            // Add new.
                            cmd.CommandText = "INSERT INTO notes(id, body, time_created) VALUES(@noteId, @body, @timeCreated)";
                            cmd.Parameters.AddWithValue("timeCreated", note.TimeCreated);
                        }
                        else
                        {
                            // Update existing.
                            cmd.CommandText = "UPDATE notes SET body = @body WHERE id = @noteId";
                        }

                        cmd.Parameters.AddWithValue("noteId", note.ID);
                        cmd.Parameters.AddWithValue("body", note.Body);

                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Adding to the link table.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "INSERT INTO case_note_links(case_id, note_id) VALUES(@caseId, @noteId)";
                        cmd.Parameters.AddWithValue("caseId", m_caseBeingEdited.ID);
                        cmd.Parameters.AddWithValue("noteId", note.ID);

                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                if (m_caseNoteBeingEdited == null)
                {
                    // Adding to the list.
                    m_caseNotes.Add(new CaseNoteViewModel(note));
                }
                else
                {
                    // Updating the view.
                    m_caseNoteBeingEdited.Model = note;
                }

                // Done.
                btnCancel_gridNote_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to add note.\nDetails: " + ex.Message);
            }
            finally
            {
                btnOK_gridNote.IsEnabled = true;
                btnCancel_gridNote.IsEnabled = true;
            }
        }

        private void btnCancel_gridNote_Click(object sender, RoutedEventArgs e)
        {
            // Clearing the user inputs.
            txtBody_gridNote.Clear();

            // Hiding the grid.
            gridNote.Visibility = Visibility.Hidden;
        }

        #endregion
    }
}
