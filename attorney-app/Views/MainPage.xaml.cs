﻿using attorney_app.Commons;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        SQLiteConnection m_dbConn;
        MainWindow m_mainwindow;

        ContactsPage m_contactsPage;
        TasksPage m_tasksPage;
        CasesPage m_casesPage;
        BillingsPage m_billingsPage;
        TemplatesPage m_templatesPage;


        public MainPage(MainWindow mainwindow)
        {
            InitializeComponent();

            m_mainwindow = mainwindow;
            Init();
        }

        async void Init()
        {
            pringLoading.IsActive = true;
            string tempDir = DatabaseAccess.TEMP_DIR;

            if (!Directory.Exists(tempDir))
            {
                // Creating the temp directory.
                Directory.CreateDirectory(tempDir);
            }

            string dbPath = Path.Combine(tempDir, "DB");

            if (!File.Exists(dbPath))
            {
                // Creating new database file.
                SQLiteConnection.CreateFile(dbPath);

                // Initializing connection.
                m_dbConn = new SQLiteConnection(string.Format("Data source={0};foreign keys=true;", dbPath));
                await m_dbConn.OpenAsync();

                // Executing init script.
                using (StreamReader reader = new StreamReader(File.Open("Resources\\Scripts\\init.sql", FileMode.Open)))
                {
                    string initScript = await reader.ReadToEndAsync();

                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = initScript;
                        await cmd.ExecuteNonQueryAsync();
                    }
                }
            }
            else
            {
                // Just opening new connection.
                m_dbConn = new SQLiteConnection(string.Format("Data source={0};foreign keys=true;", dbPath));
                await m_dbConn.OpenAsync();
            }

            // Done.
            pringLoading.IsActive = false;
        }

        private void btnContacts_Click(object sender, RoutedEventArgs e)
        {
            if (m_contactsPage == null)
            {
                m_contactsPage = new ContactsPage(m_mainwindow, m_dbConn);
            }

            m_mainwindow.Navigate(m_contactsPage);
        }

        private void btnTasks_Click(object sender, RoutedEventArgs e)
        {
            if (m_tasksPage == null)
            {
                m_tasksPage = new TasksPage(m_mainwindow, m_dbConn);
            }

            m_mainwindow.Navigate(m_tasksPage);
        }

        private void btnCases_Click(object sender, RoutedEventArgs e)
        {
            if (m_casesPage == null)
            {
                m_casesPage = new CasesPage(m_mainwindow, m_dbConn);
            }

            m_mainwindow.Navigate(m_casesPage);
        }

        private void btnBillings_Click(object sender, RoutedEventArgs e)
        {
            if (m_billingsPage == null)
            {
                m_billingsPage = new BillingsPage(m_mainwindow, m_dbConn);
            }

            m_mainwindow.Navigate(m_billingsPage);
        }

        private void btnTemplates_Click(object sender, RoutedEventArgs e)
        {
            if (m_templatesPage == null)
            {
                m_templatesPage = new TemplatesPage(m_mainwindow, m_dbConn);
            }

            m_mainwindow.Navigate(m_templatesPage);
        }
    }
}
