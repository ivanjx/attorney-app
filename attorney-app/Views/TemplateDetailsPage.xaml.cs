﻿using attorney_app.Commons;
using attorney_app.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for TemplateDetailsPage.xaml
    /// </summary>
    public partial class TemplateDetailsPage : Page
    {
        public delegate void TemplateChangedHandler();
        public event TemplateChangedHandler TemplateChanged;

        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        ObservableCollection<TemplateField> m_fields;
        Template m_templateBeingEdited;
        string m_dotxOriginalPath;


        public TemplateDetailsPage(MainWindow mainwindow, SQLiteConnection dbConn)
        {
            InitializeComponent();

            m_dbConn = dbConn;

            m_fields = new ObservableCollection<TemplateField>();
            lstFields.ItemsSource = m_fields;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (content) =>
            {
                if (content == this)
                {
                    // Clearing user inputs.
                    ClearInputs();

                    // Going back.
                    m_mainwindow.GoBack();
                }
            };
        }

        public async void Load(Template template)
        {
            m_fields.Clear();
            m_templateBeingEdited = template;

            if (template != null)
            {
                txtFileName.Text = template.FileName;
                txtDescription.Text = template.Description;

                try
                {
                    // Loading fields.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "SELECT field_name FROM template_fields WHERE template_id = @templateId";
                        cmd.Parameters.AddWithValue("templateId", template.ID);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                TemplateField field = new TemplateField();
                                field.Name = reader.GetString(0);

                                m_fields.Add(field);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to load template fields.\nDetails: " + ex.Message);
                }
            }
        }

        void ClearInputs()
        {
            // Clearing user inputs.
            m_dotxOriginalPath = string.Empty;
            m_fields.Clear();
            txtFileName.Clear();
            txtDescription.Clear();

            // Hiding grids.
            btnCancel_gridField_Click(null, null);
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select template file...";
            ofd.Multiselect = false;
            ofd.CheckFileExists = true;
            ofd.Filter = "Document Template File | *.dotx";
            ofd.FileName = string.Empty;

            bool? result = ofd.ShowDialog();
            if (result.HasValue && result.Value)
            {
                m_dotxOriginalPath = ofd.FileName;
                txtFileName.Text = Path.GetFileName(m_dotxOriginalPath);
            }
        }

        private async void btnSave_Click(object sender, RoutedEventArgs e)
        {
            btnSave.IsEnabled = false;

            try
            {
                // Checking the user inputs.
                if (m_templateBeingEdited == null && string.IsNullOrEmpty(m_dotxOriginalPath))
                {
                    throw new Exception("No template file selected");
                }

                if (string.IsNullOrEmpty(txtFileName.Text))
                {
                    throw new Exception("File name is null");
                }

                if (string.IsNullOrEmpty(txtDescription.Text))
                {
                    throw new Exception("Description is null");
                }

                if (m_fields.Count == 0)
                {
                    throw new Exception("No field");
                }

                Template template = new Template();

                if (m_templateBeingEdited == null)
                {
                    // Generating new template id.
                    template.ID = await Randomizer.RandomStringAsync(32);
                }
                else
                {
                    template.ID = m_templateBeingEdited.ID;
                }

                template.FileName = Path.GetFileName(m_dotxOriginalPath);
                template.Description = txtDescription.Text;

                // Saving to the database.
                using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                {
                    if (!string.IsNullOrEmpty(m_dotxOriginalPath))
                    {
                        // File changes detected.
                        // Uploading the dotx file.
                        template.FileID = await DatabaseAccess.UploadFile(m_dbConn, m_dotxOriginalPath, txtFileName.Text);
                    }
                    else
                    {
                        if (m_templateBeingEdited != null)
                        {
                            // No change of the file.
                            template.FileID = m_templateBeingEdited.FileID;
                        }
                    }

                    // Saving to the templates table.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        if (m_templateBeingEdited == null)
                        {
                            cmd.CommandText = "INSERT INTO templates(id, description, file_id) VALUES(@templateId, @description, @fileId)";
                        }
                        else
                        {
                            cmd.CommandText = "UPDATE templates SET description = @description, file_id = @fileId WHERE id = @templateId";
                        }

                        cmd.Parameters.AddWithValue("templateId", template.ID);
                        cmd.Parameters.AddWithValue("description", template.Description);
                        cmd.Parameters.AddWithValue("fileId", template.FileID);

                        await cmd.ExecuteNonQueryAsync();
                    }

                    if (m_templateBeingEdited == null)
                    {
                        // Saving the fields.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            for (int i = 0; i < m_fields.Count; ++i)
                            {
                                cmd.CommandText = "INSERT INTO template_fields(template_id, field_name) VALUES(@templateId, @fieldName)";
                                cmd.Parameters.AddWithValue("templateId", template.ID);
                                cmd.Parameters.AddWithValue("fieldName", m_fields[i].Name);

                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();
                            }
                        }

                        // Reloading the view.
                        ClearInputs();
                        Load(template);

                        // Telling the templates page.
                        TemplateChanged?.Invoke();
                    }

                    // Commiting transaction.
                    transaction.Commit();

                    // Done.
                    m_mainwindow.ShowMessage("Info", "Changes have been saved.");
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave.IsEnabled = true;
            }
        }

        private void btnAddField_Click(object sender, RoutedEventArgs e)
        {
            gridField.Visibility = Visibility.Visible;
        }

        private async void btnDeleteField_Click(object sender, RoutedEventArgs e)
        {
            TemplateField selitem;
            if ((selitem = lstFields.SelectedItem as TemplateField) != null)
            {
                btnDeleteField.IsEnabled = false;

                try
                {
                    if (m_templateBeingEdited != null)
                    {
                        // Deleting from the database.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "DELETE FROM template_fields WHERE template_id = @templateId AND field_name = @fieldName";
                            cmd.Parameters.AddWithValue("templateId", m_templateBeingEdited.ID);
                            cmd.Parameters.AddWithValue("fieldName", selitem.Name);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    // Removing from the list.
                    m_fields.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected field.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteField.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a field from the list first!");
            }
        }

        private async void btnOK_gridField_Click(object sender, RoutedEventArgs e)
        {
            btnOK_gridField.IsEnabled = false;
            btnCancel_gridField.IsEnabled = false;

            try
            {
                // Checking user inputs.
                if (string.IsNullOrEmpty(txtFieldName_gridField.Text))
                {
                    throw new Exception("Field name is null");
                }

                TemplateField field = new TemplateField();
                field.Name = txtFieldName_gridField.Text;

                if (m_templateBeingEdited != null)
                {
                    // Saving to the database.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "INSERT INTO template_fields(template_id, field_name) VALUES(@templateId, @fieldName)";
                        cmd.Parameters.AddWithValue("templateId", m_templateBeingEdited.ID);
                        cmd.Parameters.AddWithValue("fieldName", field.Name);

                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                // Adding to the list.
                m_fields.Add(field);

                // Done.
                btnCancel_gridField_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to add field.\nDetails: " + ex.Message);
            }
            finally
            {
                btnOK_gridField.IsEnabled = true;
                btnCancel_gridField.IsEnabled = true;
            }
        }

        private void btnCancel_gridField_Click(object sender, RoutedEventArgs e)
        {
            // Clearing user inputs.
            txtFieldName_gridField.Clear();

            // Hiding the grid.
            gridField.Visibility = Visibility.Hidden;
        }
    }
}
