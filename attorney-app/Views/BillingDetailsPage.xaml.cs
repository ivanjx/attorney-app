﻿using attorney_app.Commons;
using attorney_app.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for BillingDetailsPage.xaml
    /// </summary>
    public partial class BillingDetailsPage : Page
    {
        // View models.
        class BillingDocumentViewModel
        {
            public BillingDocument Model { get; private set; }

            public string ExpenseName
            {
                get
                {
                    return Model.ExpenseName;
                }
            }

            public string AmountString
            {
                get
                {
                    return Model.Amount.ToString() + " USD";
                }
            }


            public BillingDocumentViewModel(BillingDocument model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                BillingDocumentViewModel comp;
                if ((comp = obj as BillingDocumentViewModel) != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        public delegate void BillingAddedHandler(Billing addedBilling);
        public delegate void BillingChangedHandler(Billing changedBilling);

        public event BillingAddedHandler BillingAdded;
        public event BillingChangedHandler BillingChanged;

        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        ObservableCollection<BillingDocumentViewModel> m_billingDocs;
        Billing m_billingBeingEdited;
        bool m_isBackPermitted = true;
        double m_totalDocsBill = 0;


        public BillingDetailsPage(MainWindow mainwindow, SQLiteConnection dbConn)
        {
            InitializeComponent();

            m_dbConn = dbConn;

            m_billingDocs = new ObservableCollection<BillingDocumentViewModel>();
            lstDocuments.ItemsSource = m_billingDocs;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (content) =>
            {
                if (content == this && m_isBackPermitted)
                {
                    m_billingBeingEdited = null;

                    // Clearing user inputs.
                    ClearInputs();

                    // Going back.
                    m_mainwindow.GoBack();
                }
            };
        }

        void ClearInputs()
        {
            // Clearing the details tab.
            m_totalDocsBill = 0;
            txtName.Clear();
            dtCaseSettleDate.SelectedDate = null;
            numLiensAmount.Value = null;
            numAttorneyCompensation.Value = null;
            numClientCompensation.Value = null;
            txtTotalBill.Clear();
            chkIsPaid.IsChecked = false;

            // Clearing the documents tab.
            m_billingDocs.Clear();
            btnCancel_gridDoc_Click(null, null);
        }

        public async void Load(Billing billing)
        {
            m_billingBeingEdited = billing;

            if (billing != null)
            {
                txtName.Text = billing.Name;
                dtCaseSettleDate.SelectedDate = billing.CaseSettleDate;
                numLiensAmount.Value = billing.LiensAmount;
                numAttorneyCompensation.Value = billing.AttorneyCompensationAmount;
                numClientCompensation.Value = billing.ClientCompensationAmount;
                txtTotalBill.Text = billing.TotalBill.ToString();
                chkIsPaid.IsChecked = billing.IsPaid;
                m_totalDocsBill = billing.TotalBill + billing.ClientCompensationAmount - billing.AttorneyCompensationAmount;

                m_isBackPermitted = false;

                try
                {
                    await LoadBillingDocs();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to load billing documents.\nDetails: " + ex.Message);
                }
                finally
                {
                    m_isBackPermitted = true;
                }
            }
        }

        async Task LoadBillingDocs()
        {
            m_billingDocs.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT billing_file_links.file_id, billing_file_links.expense_name, billing_file_links.amount, files.original_name ";
                cmd.CommandText += "FROM billing_file_links, files ";
                cmd.CommandText += "WHERE billing_file_links.billing_id = @billingId AND billing_file_links.file_id = files.id";
                cmd.Parameters.AddWithValue("billingId", m_billingBeingEdited.ID);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        BillingDocument doc = new BillingDocument();
                        doc.BillingID = m_billingBeingEdited.ID;
                        doc.FileID = reader.GetString(0);
                        doc.ExpenseName = reader.GetString(1);
                        doc.Amount = reader.GetDouble(2);
                        doc.FileName = reader.GetString(3);

                        // Getting the file size.
                        using (Stream fs = DatabaseAccess.GetFileStream(doc.FileID, FileMode.Open))
                        {
                            doc.FileSize = fs.Length;
                        }

                        // Adding to the list.
                        m_billingDocs.Add(new BillingDocumentViewModel(doc));
                    }
                }
            }
        }

        private async void btnSave_Click(object sender, RoutedEventArgs e)
        {
            btnSave.IsEnabled = false;
            m_isBackPermitted = false;

            try
            {
                // Checking the user inputs.
                if (string.IsNullOrEmpty(txtName.Text))
                {
                    throw new Exception("Billing name is null");
                }

                if (!dtCaseSettleDate.SelectedDate.HasValue)
                {
                    throw new Exception("Case settle date is null");
                }

                // Adjusting the total bill.
                double totalBill = m_totalDocsBill;
                if (numAttorneyCompensation.Value.HasValue)
                {
                    totalBill += numAttorneyCompensation.Value.Value;
                }

                if (numClientCompensation.Value.HasValue)
                {
                    totalBill -= numClientCompensation.Value.Value;
                }

                // Updating the total bill.
                txtTotalBill.Text = totalBill.ToString();

                Billing billing = new Billing();

                if (m_billingBeingEdited == null)
                {
                    // Generating new id.
                    billing.ID = await Randomizer.RandomStringAsync(32);
                }
                else
                {
                    billing.ID = m_billingBeingEdited.ID;
                }

                billing.Name = txtName.Text;
                billing.CaseSettleDate = dtCaseSettleDate.SelectedDate.Value;
                billing.LiensAmount = numLiensAmount.Value.HasValue ? numLiensAmount.Value.Value : 0;
                billing.AttorneyCompensationAmount = numAttorneyCompensation.Value.HasValue ? numAttorneyCompensation.Value.Value : 0;
                billing.ClientCompensationAmount = numClientCompensation.Value.HasValue ? numClientCompensation.Value.Value : 0;
                billing.IsPaid = chkIsPaid.IsChecked.Value;
                billing.TotalBill = totalBill;

                using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                {
                    // Saving to the billings table.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        if (m_billingBeingEdited == null)
                        {
                            cmd.CommandText = "INSERT INTO billings(id, name, case_settle_date, liens_amount, attorney_compensation_amount, client_compensation_amount, total, is_paid) ";
                            cmd.CommandText += "VALUES(@billingId, @name, @caseSettleDate, @liensAmount, @attorneyCompensation, @clientCompensation, @totalBill, @isPaidInt)";
                        }
                        else
                        {
                            cmd.CommandText = "UPDATE billings SET name = @name, case_settle_date = @caseSettleDate, liens_amount = @liensAmount, ";
                            cmd.CommandText += "attorney_compensation_amount = @attorneyCompensation, client_compensation_amount = @clientCompensation, total = @totalBill, is_paid = @isPaidInt ";
                            cmd.CommandText += "WHERE id = @billingId";
                        }

                        cmd.Parameters.AddWithValue("billingId", billing.ID);
                        cmd.Parameters.AddWithValue("name", billing.Name);
                        cmd.Parameters.AddWithValue("caseSettleDate", billing.CaseSettleDate);
                        cmd.Parameters.AddWithValue("liensAmount", billing.LiensAmount);
                        cmd.Parameters.AddWithValue("attorneyCompensation", billing.AttorneyCompensationAmount);
                        cmd.Parameters.AddWithValue("clientCompensation", billing.ClientCompensationAmount);
                        cmd.Parameters.AddWithValue("totalBill", totalBill);
                        cmd.Parameters.AddWithValue("isPaidInt", billing.IsPaid ? 1 : 0);

                        await cmd.ExecuteNonQueryAsync();
                    }

                    if (m_billingBeingEdited == null)
                    {
                        // Saving the billing docs.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            for (int i = 0; i < m_billingDocs.Count; ++i)
                            {
                                // Uploading the file.
                                string fileId = await DatabaseAccess.UploadFile(m_dbConn, m_billingDocs[i].Model.FileOriginalPath, m_billingDocs[i].Model.FileName);

                                // Linking.
                                cmd.CommandText = "INSERT INTO billing_file_links(billing_id, file_id, expense_name, amount) ";
                                cmd.CommandText += "VALUES(@billingId, @fileId, @expenseName, @amount)";
                                cmd.Parameters.AddWithValue("billingId", billing.ID);
                                cmd.Parameters.AddWithValue("fileId", fileId);
                                cmd.Parameters.AddWithValue("expenseName", m_billingDocs[i].Model.ExpenseName);
                                cmd.Parameters.AddWithValue("amount", m_billingDocs[i].Model.Amount);

                                await cmd.ExecuteNonQueryAsync();
                                cmd.Reset();
                            }
                        }

                        // Reloading everything.
                        ClearInputs();
                        Load(billing);

                        // Telling the billings page.
                        BillingAdded?.Invoke(billing);
                    }
                    else
                    {
                        // Telling the billings page.
                        BillingChanged?.Invoke(billing);
                    }

                    // Commiting changes.
                    transaction.Commit();
                }

                // Done.
                m_mainwindow.ShowMessage("Info", "Changes have been saved.");
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave.IsEnabled = true;
                m_isBackPermitted = true;
            }
        }

        #region DOCUMENTS_TAB

        private void btnAddDocument_Click(object sender, RoutedEventArgs e)
        {
            gridDoc.Visibility = Visibility.Visible;
        }

        private async void btnDeleteDocument_Click(object sender, RoutedEventArgs e)
        {
            BillingDocumentViewModel selitem;
            if ((selitem = lstDocuments.SelectedItem as BillingDocumentViewModel) != null)
            {
                btnDeleteDocument.IsEnabled = false;

                try
                {
                    if (m_billingBeingEdited != null)
                    {
                        // Just delete the file and the link table will follow.
                        using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                        {
                            // Deleting the file.
                            await DatabaseAccess.DeleteFile(m_dbConn, selitem.Model.FileID);

                            // Decreasing total amount from the database.
                            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                            {
                                cmd.CommandText = "UPDATE billings SET total = total - @docAmount WHERE id = @billingId";
                                cmd.Parameters.AddWithValue("billingId", m_billingBeingEdited.ID);
                                cmd.Parameters.AddWithValue("docAmount", selitem.Model.Amount);

                                await cmd.ExecuteNonQueryAsync();
                            }

                            // Commiting changes.
                            transaction.Commit();
                        }
                    }

                    // Removing from the list.
                    m_billingDocs.Remove(selitem);

                    // Decreasing total amount.
                    m_totalDocsBill -= selitem.Model.Amount;
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected document.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteDocument.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a document from the list first!");
            }
        }

        private async void btnDownloadDocument_Click(object sender, RoutedEventArgs e)
        {
            BillingDocumentViewModel selitem;
            if ((selitem = lstDocuments.SelectedItem as BillingDocumentViewModel) != null)
            {
                if (m_billingBeingEdited == null)
                {
                    Process.Start(selitem.Model.FileOriginalPath);
                }
                else
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Title = "Select download file location...";
                    sfd.FileName = selitem.Model.FileName;
                    sfd.DefaultExt = Path.GetExtension(selitem.Model.FileName);

                    bool? result = sfd.ShowDialog();
                    if (result.HasValue && result.Value)
                    {
                        string targetPath = sfd.FileName;

                        using (Stream inStream = DatabaseAccess.GetFileStream(selitem.Model.FileID, FileMode.Open))
                        using (Stream outStream = File.Open(targetPath, FileMode.Create))
                        {
                            await inStream.CopyToAsync(outStream);
                        }

                        // Opening the file.
                        Process.Start(targetPath);
                    }
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a document from the list first!");
            }
        }

        private void btnBrowseFile_gridDoc_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select a file...";
            ofd.FileName = string.Empty;
            ofd.Multiselect = false;
            ofd.CheckFileExists = true;

            bool? result = ofd.ShowDialog();
            if (result.HasValue && result.Value)
            {
                txtFilePath_gridDoc.Text = ofd.FileName;
            }
        }

        private async void btnOK_gridDoc_Click(object sender, RoutedEventArgs e)
        {
            btnOK_gridDoc.IsEnabled = false;
            btnCancel_gridDoc.IsEnabled = false;

            try
            {
                // Checking the user inputs.
                if (string.IsNullOrEmpty(txtFilePath_gridDoc.Text))
                {
                    throw new Exception("No file selected");
                }

                if (string.IsNullOrEmpty(txtExpenseName_gridDoc.Text))
                {
                    throw new Exception("Expense name is null");
                }

                if (!numAmount_gridDoc.Value.HasValue)
                {
                    throw new Exception("Amount was no specified");
                }

                BillingDocument doc = new BillingDocument();
                doc.FileOriginalPath = txtFilePath_gridDoc.Text;
                doc.FileName = Path.GetFileName(doc.FileOriginalPath);
                doc.FileSize = new FileInfo(doc.FileOriginalPath).Length;
                doc.ExpenseName = txtExpenseName_gridDoc.Text;
                doc.Amount = numAmount_gridDoc.Value.Value;

                if (m_billingBeingEdited != null)
                {
                    doc.BillingID = m_billingBeingEdited.ID;

                    using (SQLiteTransaction transaction = m_dbConn.BeginTransaction())
                    {
                        // Uploading the file.
                        doc.FileID = await DatabaseAccess.UploadFile(m_dbConn, doc.FileOriginalPath, doc.FileName);

                        // Linking.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "INSERT INTO billing_file_links(billing_id, file_id, expense_name, amount) ";
                            cmd.CommandText += "VALUES(@billingId, @fileId, @expenseName, @amount)";
                            cmd.Parameters.AddWithValue("billingId", m_billingBeingEdited.ID);
                            cmd.Parameters.AddWithValue("fileId", doc.FileID);
                            cmd.Parameters.AddWithValue("expenseName", doc.ExpenseName);
                            cmd.Parameters.AddWithValue("amount", doc.Amount);

                            await cmd.ExecuteNonQueryAsync();
                        }

                        // Updating the total bill.
                        using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                        {
                            cmd.CommandText = "UPDATE billings SET total = total + @docAmount WHERE id = @billingId";
                            cmd.Parameters.AddWithValue("billingId", m_billingBeingEdited.ID);
                            cmd.Parameters.AddWithValue("docAmount", doc.Amount);

                            await cmd.ExecuteNonQueryAsync();
                        }

                        // Commiting changes.
                        transaction.Commit();
                    }
                }

                // Adding to the list.
                m_billingDocs.Add(new BillingDocumentViewModel(doc));

                // Increasing total bill.
                m_totalDocsBill += doc.Amount;

                // Done.
                btnCancel_gridDoc_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to add billing document.\nDetails: " + ex.Message);
            }
            finally
            {
                btnOK_gridDoc.IsEnabled = true;
                btnCancel_gridDoc.IsEnabled = true;
            }
        }

        private void btnCancel_gridDoc_Click(object sender, RoutedEventArgs e)
        {
            // Clearing user inputs.
            txtFilePath_gridDoc.Clear();
            txtExpenseName_gridDoc.Clear();
            numAmount_gridDoc.Value = null;

            // Hiding the grid.
            gridDoc.Visibility = Visibility.Hidden;
        }

        #endregion
    }
}
