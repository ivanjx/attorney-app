﻿using attorney_app.Commons;
using attorney_app.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for TemplatesPage.xaml
    /// </summary>
    public partial class TemplatesPage : Page
    {
        // View models.
        class TemplateViewModel
        {
            public Template Model { get; private set; }

            public string FileName
            {
                get
                {
                    return Model.FileName;
                }
            }

            public string Description
            {
                get
                {
                    if (Model.Description.Length > 60)
                    {
                        return string.Format("{0}...", Model.Description.Trim().Substring(0, 30).Replace('\n', ' ').Replace('\r', ' '));
                    }
                    else
                    {
                        return Model.Description;
                    }
                }
            }


            public TemplateViewModel(Template model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                TemplateViewModel comp;
                if ((comp = obj as TemplateViewModel) != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        ObservableCollection<TemplateViewModel> m_templates;
        TemplateDetailsPage m_templateDetailsPage;
        NewDocumentPage m_newDocumentPage;


        public TemplatesPage(MainWindow mainwindow, SQLiteConnection dbConn)
        {
            InitializeComponent();

            m_dbConn = dbConn;

            m_templates = new ObservableCollection<TemplateViewModel>();
            lstTemplates.ItemsSource = m_templates;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (content) =>
            {
                if (content == this)
                {
                    // Going back.
                    m_mainwindow.GoBack();
                }
            };
            m_mainwindow.PageLoaded += async (content) =>
            {
                if (content == this)
                {
                    try
                    {
                        await LoadTemplates();
                    }
                    catch (Exception ex)
                    {
                        m_mainwindow.ShowMessage("Error", "Unable to load templates.\nDetails: " + ex.Message);
                    }
                }
            };
        }

        async Task LoadTemplates()
        {
            m_templates.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT templates.id, templates.description, templates.file_id, files.original_name ";
                cmd.CommandText += "FROM templates, files ";
                cmd.CommandText += "WHERE templates.file_id = files.id";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        Template template = new Template();
                        template.ID = reader.GetString(0);
                        template.Description = reader.GetString(1);
                        template.FileID = reader.GetString(2);
                        template.FileName = reader.GetString(3);

                        // Adding to the list.
                        m_templates.Add(new TemplateViewModel(template));
                    }
                }
            }
        }

        void InitTemplateDetailsPage()
        {
            if (m_templateDetailsPage != null)
            {
                throw new Exception("Template details page was initialized");
            }

            m_templateDetailsPage = new TemplateDetailsPage(m_mainwindow, m_dbConn);
            m_templateDetailsPage.TemplateChanged += async () =>
            {
                try
                {
                    await LoadTemplates();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to load templates.\nDetails: " + ex.Message);
                }
            };
        }

        private void btnAddTemplate_Click(object sender, RoutedEventArgs e)
        {
            if (m_templateDetailsPage == null)
            {
                InitTemplateDetailsPage();
            }

            // Loading view.
            m_templateDetailsPage.Load(null);

            // Navigating.
            m_mainwindow.Navigate(m_templateDetailsPage);
        }

        private async void btnDeleteTemplate_Click(object sender, RoutedEventArgs e)
        {
            TemplateViewModel selitem;
            if ((selitem = lstTemplates.SelectedItem as TemplateViewModel) != null)
            {
                btnDeleteTemplate.IsEnabled = false;

                try
                {
                    // Deleting the file will delete from templates and template_fields too.
                    await DatabaseAccess.DeleteFile(m_dbConn, selitem.Model.FileID);

                    // Removing from the list.
                    m_templates.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected template.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteTemplate.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a template from the list first!");
            }
        }

        private void btnEditTemplate_Click(object sender, RoutedEventArgs e)
        {
            TemplateViewModel selitem;
            if ((selitem = lstTemplates.SelectedItem as TemplateViewModel) != null)
            {
                if (m_templateDetailsPage == null)
                {
                    InitTemplateDetailsPage();
                }

                // Loading the view.
                m_templateDetailsPage.Load(selitem.Model);

                // Navigating.
                m_mainwindow.Navigate(m_templateDetailsPage);
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a template from the list first!");
            }
        }

        private void btnNewDocument_Click(object sender, RoutedEventArgs e)
        {
            TemplateViewModel selitem;
            if ((selitem = lstTemplates.SelectedItem as TemplateViewModel) != null)
            {
                if (m_newDocumentPage == null)
                {
                    m_newDocumentPage = new NewDocumentPage(m_mainwindow, m_dbConn);
                }

                // Loading the view.
                m_newDocumentPage.Load(selitem.Model);

                // Navigating.
                m_mainwindow.Navigate(m_newDocumentPage);
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a template from the list first!");
            }
        }
    }
}
