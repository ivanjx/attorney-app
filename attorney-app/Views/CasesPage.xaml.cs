﻿using attorney_app.Commons;
using attorney_app.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for CasesPage.xaml
    /// </summary>
    public partial class CasesPage : Page
    {
        // Required view models.
        class CaseViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            Case model;

            public Case Model
            {
                get
                {
                    return model;
                }
                set
                {
                    model = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ID"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DateAddedString"));
                }
            }

            public string ID
            {
                get
                {
                    return Model.ID;
                }
            }

            public string Name
            {
                get
                {
                    return Model.Name;
                }
            }

            public string TimeAddedString
            {
                get
                {
                    return model.AddedTime.ToString();
                }
            }


            public CaseViewModel(Case model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                CaseViewModel comp;
                if ((comp = obj as CaseViewModel) != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        MainWindow m_mainwindow;
        CaseDetailsPage m_caseDetailsPage;
        SQLiteConnection m_dbConn;
        ObservableCollection<CaseViewModel> m_onGoingCases;
        ObservableCollection<CaseViewModel> m_doneCases;
        ObservableCollection<CaseType> m_caseTypes;
        bool m_isBackPermitted = true;


        public CasesPage(MainWindow mainwindow, SQLiteConnection dbConn)
        {
            InitializeComponent();

            m_dbConn = dbConn;

            m_onGoingCases = new ObservableCollection<CaseViewModel>();
            lstCases_OnGoing.ItemsSource = m_onGoingCases;

            m_doneCases = new ObservableCollection<CaseViewModel>();
            lstCases_Done.ItemsSource = m_doneCases;

            m_caseTypes = new ObservableCollection<CaseType>();
            lstCaseTypes.ItemsSource = m_caseTypes;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (content) =>
            {
                if (content == this && m_isBackPermitted)
                {
                    // Hiding the grid.
                    btnCancel_gridCaseType_Click(null, null);

                    // Going back.
                    m_mainwindow.GoBack();
                }
            };
            m_mainwindow.PageLoaded += async (content) =>
            {
                void InitCaseDetailsPage()
                {
                    if (m_caseDetailsPage != null)
                    {
                        throw new Exception("Case details page was initialized");
                    }

                    m_caseDetailsPage = new CaseDetailsPage(m_mainwindow, m_dbConn);
                    m_caseDetailsPage.CaseAdded += (addedCase) =>
                    {
                        // Adding to the list.
                        CaseViewModel vm = new CaseViewModel(addedCase);

                        if (addedCase.IsDone)
                        {
                            m_doneCases.Add(vm);
                        }
                        else
                        {
                            m_onGoingCases.Add(vm);
                        }
                    };
                    m_caseDetailsPage.CaseChanged += async (changedCase) =>
                    {
                        m_isBackPermitted = false;

                        try
                        {
                            // Loading the cases.
                            await LoadCases();
                        }
                        catch (Exception ex)
                        {
                            m_mainwindow.ShowMessage("Error", "Unable to load cases.\nDetails: " + ex.Message);
                        }
                        finally
                        {
                            m_isBackPermitted = true;
                        }
                    };
                }

                if (content == this)
                {
                    if (m_caseDetailsPage == null)
                    {
                        InitCaseDetailsPage();
                    }

                    try
                    {
                        m_isBackPermitted = false;

                        // Loading case types.
                        await LoadCaseTypes();

                        // Loading the cases.
                        await LoadCases();
                    }
                    catch (Exception ex)
                    {
                        m_mainwindow.ShowMessage("Error", "Unable to load cases.\nDetails: " + ex.Message);
                    }
                    finally
                    {
                        m_isBackPermitted = true;
                    }
                }
            };
        }

        async Task LoadCaseTypes()
        {
            m_caseTypes.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT * FROM case_types";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        CaseType caseType = new CaseType();
                        caseType.ID = reader.GetInt32(0);
                        caseType.Name = reader.GetString(1);

                        m_caseTypes.Add(caseType);
                    }
                }
            }
        }

        async Task LoadCases()
        {
            m_onGoingCases.Clear();
            m_doneCases.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT cases.id, cases.name, cases.date_retainer, cases.date_accident, cases.date_max, ";
                cmd.CommandText += "cases.date_app_deadline, cases.trial_venue, cases.description, cases.liability_limits, ";
                cmd.CommandText += "cases.is_done, cases.time_added, cases.type_id, case_types.value ";
                cmd.CommandText += "FROM cases, case_types WHERE cases.type_id = case_types.id ORDER BY time_added ASC";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        Case acase = new Case();
                        acase.ID = reader.GetString(0);
                        acase.Name = reader.GetString(1);
                        acase.RetainerDate = await reader.IsDBNullAsync(2) ? default(DateTime) : reader.GetDateTime(2);
                        acase.AccidentDate = await reader.IsDBNullAsync(3) ? default(DateTime) : reader.GetDateTime(3);
                        acase.CaseMaxDate = await reader.IsDBNullAsync(4) ? default(DateTime) : reader.GetDateTime(4);
                        acase.ApplicationDeadlineDate = await reader.IsDBNullAsync(5) ? default(DateTime) : reader.GetDateTime(5);
                        acase.TrialVenue = await reader.IsDBNullAsync(6) ? string.Empty : reader.GetString(6);
                        acase.Description = await reader.IsDBNullAsync(7) ? string.Empty : reader.GetString(7);
                        acase.LiabilityLimits = await reader.IsDBNullAsync(8) ? string.Empty : reader.GetString(8);
                        acase.IsDone = reader.GetInt32(9) == 1;
                        acase.AddedTime = reader.GetDateTime(10);

                        CaseType type = new CaseType();
                        type.ID = reader.GetInt32(11);
                        type.Name = reader.GetString(12);
                        acase.Type = type;

                        CaseViewModel casevm = new CaseViewModel(acase);

                        if (acase.IsDone)
                        {
                            m_doneCases.Add(casevm);
                        }
                        else
                        {
                            m_onGoingCases.Add(casevm);
                        }
                    }
                }
            }
        }

        async Task DeleteCase(Case acase)
        {
            // Deleting documents.
            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT file_id FROM case_file_links WHERE case_id = @caseId";
                cmd.Parameters.AddWithValue("caseId", acase.ID);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        await DatabaseAccess.DeleteFile(m_dbConn, reader.GetString(0));
                    }
                }
            }

            // Deleting notes.
            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT note_id FROM case_note_links WHERE case_id = @caseId";
                cmd.Parameters.AddWithValue("caseId", acase.ID);

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        string noteId = reader.GetString(0);

                        using (SQLiteCommand deleteCmd = new SQLiteCommand(m_dbConn))
                        {
                            deleteCmd.CommandText = "DELETE FROM notes WHERE id = @noteId";
                            deleteCmd.Parameters.AddWithValue("noteId", noteId);

                            await deleteCmd.ExecuteNonQueryAsync();
                        }
                    }
                }
            }

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "DELETE FROM cases WHERE id = @caseId";
                cmd.Parameters.AddWithValue("caseId", acase.ID);

                await cmd.ExecuteNonQueryAsync();
            }
        }

        void EditCase(Case acase)
        {
            // Preparing the view.
            m_caseDetailsPage.Load(acase, m_caseTypes);

            // Navigating to the page.
            m_mainwindow.Navigate(m_caseDetailsPage);
        }

        #region ON_GOING_TAB

        private void btnAddCase_Click(object sender, RoutedEventArgs e)
        {
            // Loading the view.
            m_caseDetailsPage.Load(null, m_caseTypes);

            // Navigating.
            m_mainwindow.Navigate(m_caseDetailsPage);
        }

        private async void btnDeleteCase_OnGoing_Click(object sender, RoutedEventArgs e)
        {
            CaseViewModel selitem;
            if ((selitem = lstCases_OnGoing.SelectedItem as CaseViewModel) != null)
            {
                btnDeleteCase_OnGoing.IsEnabled = false;

                try
                {
                    await DeleteCase(selitem.Model);

                    // Removing from the list.
                    m_onGoingCases.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected case.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteCase_OnGoing.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a case from the list first!");
            }
        }

        private void btnEditCase_OnGoing_Click(object sender, RoutedEventArgs e)
        {
            CaseViewModel selitem;
            if ((selitem = lstCases_OnGoing.SelectedItem as CaseViewModel) != null)
            {
                EditCase(selitem.Model);
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a case from the list first!");
            }
        }

        #endregion

        #region DONE_TAB

        private async void btnDeleteCase_Done_Click(object sender, RoutedEventArgs e)
        {
            CaseViewModel selitem;
            if ((selitem = lstCases_Done.SelectedItem as CaseViewModel) != null)
            {
                btnDeleteCase_Done.IsEnabled = false;

                try
                {
                    await DeleteCase(selitem.Model);

                    // Removing from the list.
                    m_doneCases.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected case.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteCase_Done.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a case from the list first!");
            }
        }

        private void btnEditCase_Done_Click(object sender, RoutedEventArgs e)
        {
            CaseViewModel selitem;
            if ((selitem = lstCases_Done.SelectedItem as CaseViewModel) != null)
            {
                EditCase(selitem.Model);
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a case from the list first!");
            }
        }

        #endregion

        #region TYPES_TAB

        private void btnAddCaseType_Click(object sender, RoutedEventArgs e)
        {
            gridCaseType.Visibility = Visibility.Visible;
        }

        private async void btnDeleteCaseType_Click(object sender, RoutedEventArgs e)
        {
            CaseType selitem;
            if ((selitem = lstCaseTypes.SelectedItem as CaseType) != null)
            {
                btnDeleteCaseType.IsEnabled = false;

                try
                {
                    // Deleting from the database.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "DELETE FROM case_types WHERE id = @typeId";
                        cmd.Parameters.AddWithValue("typeId", selitem.ID);

                        await cmd.ExecuteNonQueryAsync();
                    }
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected case type.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteCaseType.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a case type from the list first!");
            }
        }

        private async void btnOK_gridCaseType_Click(object sender, RoutedEventArgs e)
        {
            btnOK_gridCaseType.IsEnabled = false;
            btnCancel_gridCaseType.IsEnabled = false;

            try
            {
                // Checking the user inputs.
                if (string.IsNullOrEmpty(txtName_gridCaseType.Text))
                {
                    throw new Exception("Case type name is null");
                }

                CaseType caseType = new CaseType();
                caseType.ID = Randomizer.RandomInt(0, int.MaxValue);
                caseType.Name = txtName_gridCaseType.Text;

                // Saving to the database.
                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                {
                    cmd.CommandText = "INSERT INTO case_types(id, value) VALUES(@id, @value)";
                    cmd.Parameters.AddWithValue("id", caseType.ID);
                    cmd.Parameters.AddWithValue("value", caseType.Name);

                    await cmd.ExecuteNonQueryAsync();
                }

                // Adding to the list.
                m_caseTypes.Add(caseType);

                // Done.
                btnCancel_gridCaseType_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to add new case type.\nDetails: " + ex.Message);
            }
            finally
            {
                btnOK_gridCaseType.IsEnabled = true;
                btnCancel_gridCaseType.IsEnabled = true;
            }
        }

        private void btnCancel_gridCaseType_Click(object sender, RoutedEventArgs e)
        {
            // Clearing the user inputs.
            txtName_gridCaseType.Clear();

            // Hiding the grid.
            gridCaseType.Visibility = Visibility.Hidden;
        }

        #endregion
    }
}
