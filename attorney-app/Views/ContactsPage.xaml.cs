﻿using attorney_app.Commons;
using attorney_app.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace attorney_app.Views
{
    /// <summary>
    /// Interaction logic for ContactsPage.xaml
    /// </summary>
    public partial class ContactsPage : Page
    {
        // Required view models.
        class ContactViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            Contact model;

            public Contact Model
            {
                get
                {
                    return model;
                }
                set
                {
                    model = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("County"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("State"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("GroupName"));
                }
            }

            public string Name
            {
                get
                {
                    return Model.Name;
                }
            }

            public string County
            {
                get
                {
                    return Model.County;
                }
            }

            public string State
            {
                get
                {
                    return Model.State;
                }
            }

            public string GroupName
            {
                get
                {
                    return Model.Group.Name;
                }
            }


            public ContactViewModel(Contact model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                ContactViewModel comp;
                if ((comp = obj as ContactViewModel) != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        class County
        {
            public string Name { get; set; }
        }

        // Page definition.
        const int MAX_PHOTO_DIMENSIONS = 500;

        MainWindow m_mainwindow;
        SQLiteConnection m_dbConn;
        ObservableCollection<ContactViewModel> m_contacts;
        ObservableCollection<ContactGroup> m_contactGroups;
        ContactViewModel m_contactBeingEdited;
        string m_tempOriginalPhotoPath;
        bool m_isBackPermitted = true;


        public ContactsPage(MainWindow mainwindow, SQLiteConnection dbConn)
        {
            InitializeComponent();

            m_contactGroups = new ObservableCollection<ContactGroup>();
            lstContactGroups.ItemsSource = m_contactGroups;
            cmbGroup_gridContact.ItemsSource = m_contactGroups;

            m_dbConn = dbConn;
            LoadComboBoxes();

            m_contacts = new ObservableCollection<ContactViewModel>();
            lstContacts.ItemsSource = m_contacts;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += (content) =>
            {
                if (content == this && m_isBackPermitted)
                {
                    // Hiding the grids.
                    btnCancel_gridContact_Click(null, null);
                    btnCancel_gridGroup_Click(null, null);

                    // Going back.
                    m_mainwindow.GoBack();
                }
            };
            m_mainwindow.PageLoaded += async (content) =>
            {
                if (content == this)
                {
                    m_isBackPermitted = false;

                    try
                    {
                        // Loading contacts.
                        await LoadContacts();
                    }
                    catch (Exception ex)
                    {
                        m_mainwindow.ShowMessage("Error", "Unable to load contacts.\nDetails: " + ex.Message);
                    }
                    finally
                    {
                        m_isBackPermitted = true;
                    }
                }
            };
        }

        async Task LoadContactGroups()
        {
            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT * FROM contact_groups";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        ContactGroup contactGroup = new ContactGroup();
                        contactGroup.ID = reader.GetInt32(0);
                        contactGroup.Name = reader.GetString(1);

                        m_contactGroups.Add(contactGroup);
                    }
                }
            }
        }

        async Task LoadCounties()
        {
            List<County> counties = new List<County>();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT * FROM ny_counties";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        County county = new County();
                        county.Name = reader.GetString(0);

                        counties.Add(county);
                    }
                }
            }

            cmbCounty_gridContact.ItemsSource = counties;
        }

        async void LoadComboBoxes()
        {
            await LoadContactGroups();
            await LoadCounties();

            // Loading the gender combobox.
            cmbGender_gridContact.Items.Add("MALE");
            cmbGender_gridContact.Items.Add("FEMALE");
        }

        async Task LoadContacts()
        {
            m_contacts.Clear();

            using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
            {
                cmd.CommandText = "SELECT contacts.id, contacts.name, contacts.is_male, contacts.birth_date, contacts.ssn, contact_groups.id, contact_groups.value, contacts.photo_id, ";
                cmd.CommandText += "contacts.address, contacts.county, contacts.state, contacts.zip_code, ";
                cmd.CommandText += "contacts.home_phone, contacts.work_phone, contacts.cell_phone, contacts.fax, contacts.email ";
                cmd.CommandText += "FROM contacts, contact_groups ";
                cmd.CommandText += "WHERE contacts.group_id = contact_groups.id ORDER BY contacts.name ASC";

                using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        Contact contact = new Contact();

                        // Personal info.
                        contact.ID = reader.GetString(0);
                        contact.Name = reader.GetString(1);
                        contact.IsMale = reader.GetInt32(2) == 1;
                        contact.BirthDate = await reader.IsDBNullAsync(3) ? default(DateTime) : reader.GetDateTime(3);
                        contact.SSN = await reader.IsDBNullAsync(4) ? string.Empty : reader.GetString(4);

                        ContactGroup group = new ContactGroup();
                        group.ID = reader.GetInt32(5);
                        group.Name = reader.GetString(6);
                        contact.Group = group;

                        contact.PhotoID = await reader.IsDBNullAsync(7) ? string.Empty : reader.GetString(7);

                        // Address info.
                        contact.Address = await reader.IsDBNullAsync(8) ? string.Empty : reader.GetString(8);
                        contact.County = await reader.IsDBNullAsync(9) ? string.Empty : reader.GetString(9);
                        contact.State = await reader.IsDBNullAsync(10) ? string.Empty : reader.GetString(10);
                        contact.ZipCode = await reader.IsDBNullAsync(11) ? string.Empty : reader.GetString(11);

                        // Contact info.
                        contact.HomePhone = await reader.IsDBNullAsync(12) ? string.Empty : reader.GetString(12);
                        contact.WorkPhone = await reader.IsDBNullAsync(13) ? string.Empty : reader.GetString(13);
                        contact.CellPhone = await reader.IsDBNullAsync(14) ? string.Empty : reader.GetString(14);
                        contact.Fax = await reader.IsDBNullAsync(15) ? string.Empty : reader.GetString(15);
                        contact.Email = await reader.IsDBNullAsync(16) ? string.Empty : reader.GetString(16);

                        // Adding to the list.
                        m_contacts.Add(new ContactViewModel(contact));
                    }
                }
            }
        }

        private void btnAddContact_Click(object sender, RoutedEventArgs e)
        {
            gridContact.Visibility = Visibility.Visible;
        }

        private async void btnDeleteContact_Click(object sender, RoutedEventArgs e)
        {
            ContactViewModel selitem;
            if ((selitem = lstContacts.SelectedItem as ContactViewModel) != null)
            {
                btnDeleteContact.IsEnabled = false;

                try
                {
                    // Deleting from the database.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "DELETE FROM contacts WHERE id = @contactId";
                        cmd.Parameters.AddWithValue("contactId", selitem.Model.ID);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Removing from the list.
                    m_contacts.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected contact.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteContact.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a contact from the list first!");
            }
        }

        private async void btnEditContact_Click(object sender, RoutedEventArgs e)
        {
            ContactViewModel selitem;
            if ((selitem = lstContacts.SelectedItem as ContactViewModel) != null)
            {
                m_contactBeingEdited = selitem;

                // Filling the personal info.
                txtName_gridContact.Text = m_contactBeingEdited.Name;

                if (m_contactBeingEdited.Model.IsMale)
                {
                    cmbGender_gridContact.SelectedItem = "MALE";
                }
                else
                {
                    cmbGender_gridContact.SelectedItem = "FEMALE";
                }

                if (m_contactBeingEdited.Model.BirthDate != default(DateTime))
                {
                    dtBirthDate_gridContact.SelectedDate = m_contactBeingEdited.Model.BirthDate;
                }

                cmbGroup_gridContact.SelectedItem = m_contactBeingEdited.Model.Group;
                txtSSN_gridContact.Text = m_contactBeingEdited.Model.SSN;

                // Loading the photo.
                if (!string.IsNullOrEmpty(m_contactBeingEdited.Model.PhotoID))
                {
                    using (Stream imgStream = DatabaseAccess.GetFileStream(m_contactBeingEdited.Model.PhotoID, FileMode.Open))
                    {
                        BitmapImage img = await Imaging.LoadImageFromStreamAsync(imgStream, MAX_PHOTO_DIMENSIONS);
                        imgPhoto_gridContact.Source = img;
                    }
                }

                // Filling the address info.
                txtAddress_gridContact.Text = m_contactBeingEdited.Model.Address;
                cmbCounty_gridContact.Text = m_contactBeingEdited.Model.County;
                txtState_gridContact.Text = m_contactBeingEdited.Model.State;
                txtZipCode_gridContact.Text = m_contactBeingEdited.Model.ZipCode;

                // Filling the contact info.
                txtHomePhone_gridContact.Text = m_contactBeingEdited.Model.HomePhone;
                txtWorkPhone_gridContact.Text = m_contactBeingEdited.Model.WorkPhone;
                txtCellPhone_gridContact.Text = m_contactBeingEdited.Model.CellPhone;
                txtFax_gridContact.Text = m_contactBeingEdited.Model.Fax;
                txtEmail_gridContact.Text = m_contactBeingEdited.Model.Email;

                // Showing the grid.
                gridContact.Visibility = Visibility.Visible;
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a contact from the list first!");
            }
        }

        private async void photoContainer_gridContact_Click(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = string.Empty;
            ofd.Multiselect = false;
            ofd.Filter = "Image Files|*.bmp;*.jpg;*.png";

            bool? result = ofd.ShowDialog();
            if (result.HasValue && result.Value)
            {
                m_tempOriginalPhotoPath = ofd.FileName;

                // Loading the photo.
                using (Stream imgStream = File.Open(m_tempOriginalPhotoPath, FileMode.Open))
                {
                    BitmapImage bmp = await Imaging.LoadImageFromStreamAsync(imgStream, MAX_PHOTO_DIMENSIONS);
                    imgPhoto_gridContact.Source = bmp;
                }
            }
        }

        private async void btnOK_gridContact_Click(object sender, RoutedEventArgs e)
        {
            m_isBackPermitted = false;
            btnOK_gridContact.IsEnabled = false;
            btnCancel_gridContact.IsEnabled = false;

            try
            {
                // Checking user inputs.
                if (string.IsNullOrEmpty(txtName_gridContact.Text))
                {
                    throw new Exception("Name is null");
                }

                if (cmbGroup_gridContact.SelectedItem == null)
                {
                    throw new Exception("No group selected");
                }

                Contact contact = new Contact();

                if (m_contactBeingEdited == null)
                {
                    // Generating new ID.
                    contact.ID = await Randomizer.RandomStringAsync(32);
                }
                else
                {
                    contact.ID = m_contactBeingEdited.Model.ID;
                }

                // Personal info.
                contact.Name = txtName_gridContact.Text;
                contact.IsMale = cmbGender_gridContact.SelectedItem as string == "MALE";
                contact.BirthDate = dtBirthDate_gridContact.SelectedDate.HasValue ? dtBirthDate_gridContact.SelectedDate.Value : default(DateTime);
                contact.Group = cmbGroup_gridContact.SelectedItem as ContactGroup;
                contact.SSN = txtSSN_gridContact.Text;

                if (!string.IsNullOrEmpty(m_tempOriginalPhotoPath))
                {
                    contact.PhotoID = await DatabaseAccess.UploadFile(m_dbConn, m_tempOriginalPhotoPath, Path.GetFileName(m_tempOriginalPhotoPath));
                }
                else if (m_contactBeingEdited != null)
                {
                    contact.PhotoID = m_contactBeingEdited.Model.PhotoID;
                }

                // Address info.
                contact.Address = txtAddress_gridContact.Text;
                contact.County = cmbCounty_gridContact.Text.ToUpper();
                contact.State = txtState_gridContact.Text.ToUpper();
                contact.ZipCode = txtZipCode_gridContact.Text;

                // Contact info.
                contact.HomePhone = txtHomePhone_gridContact.Text;
                contact.WorkPhone = txtWorkPhone_gridContact.Text;
                contact.CellPhone = txtCellPhone_gridContact.Text;
                contact.Fax = txtFax_gridContact.Text;
                contact.Email = txtEmail_gridContact.Text;

                // Saving to the database.
                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                {
                    if (m_contactBeingEdited == null)
                    {
                        // Create new.
                        cmd.CommandText = "INSERT INTO contacts(id, name, is_male, birth_date, group_id, ssn, photo_id, ";
                        cmd.CommandText += "address, county, state, zip_code, ";
                        cmd.CommandText += "home_phone, work_phone, cell_phone, fax, email) VALUES(";
                        cmd.CommandText += "@contactId, @name, @isMaleInt, @birthDate, @groupId, @ssn, @photoId, ";
                        cmd.CommandText += "@address, @county, @state, @zipCode, ";
                        cmd.CommandText += "@homePhone, @workPhone, @cellPhone, @fax, @email)";
                    }
                    else
                    {
                        cmd.CommandText = "UPDATE contacts SET ";
                        cmd.CommandText += "name = @name, is_male = @isMaleInt, birth_date = @birthDate, group_id = @groupId, ssn = @ssn, photo_id = @photoId, ";
                        cmd.CommandText += "address = @address, county = @county, state = @state, zip_code = @zipCode, ";
                        cmd.CommandText += "home_phone = @homePhone, work_phone = @workPhone, cell_phone = @cellPhone, fax = @fax, email = @email ";
                        cmd.CommandText += "WHERE id = @contactId";
                    }

                    cmd.Parameters.AddWithValue("contactId", contact.ID);
                    cmd.Parameters.AddWithValue("name", contact.Name);
                    cmd.Parameters.AddWithValue("isMaleInt", contact.IsMale ? 1 : 0);
                    cmd.Parameters.AddWithValue("birthDate", contact.BirthDate);
                    cmd.Parameters.AddWithValue("groupId", contact.Group.ID);
                    cmd.Parameters.AddWithValue("ssn", contact.SSN);
                    cmd.Parameters.AddWithValue("photoId", contact.PhotoID);

                    cmd.Parameters.AddWithValue("address", contact.Address);
                    cmd.Parameters.AddWithValue("county", contact.County);
                    cmd.Parameters.AddWithValue("state", contact.State);
                    cmd.Parameters.AddWithValue("zipCode", contact.ZipCode);

                    cmd.Parameters.AddWithValue("homePhone", contact.HomePhone);
                    cmd.Parameters.AddWithValue("workPhone", contact.WorkPhone);
                    cmd.Parameters.AddWithValue("cellPhone", contact.CellPhone);
                    cmd.Parameters.AddWithValue("fax", contact.Fax);
                    cmd.Parameters.AddWithValue("email", contact.Email);

                    await cmd.ExecuteNonQueryAsync();
                }

                if (m_contactBeingEdited == null)
                {
                    // Reloading the whole list (to apply sorting).
                    await LoadContacts();
                }
                else
                {
                    // Refreshing the view.
                    m_contactBeingEdited.Model = contact;
                }

                // Done.
                btnCancel_gridContact_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                m_isBackPermitted = true;
                btnOK_gridContact.IsEnabled = true;
                btnCancel_gridContact.IsEnabled = true;
            }
        }

        private void btnCancel_gridContact_Click(object sender, RoutedEventArgs e)
        {
            m_contactBeingEdited = null;

            // Clearing user inputs.
            m_tempOriginalPhotoPath = string.Empty;
            txtName_gridContact.Clear();
            cmbGender_gridContact.SelectedItem = null;
            dtBirthDate_gridContact.SelectedDate = null;
            cmbGroup_gridContact.SelectedItem = null;
            txtSSN_gridContact.Clear();
            imgPhoto_gridContact.Source = null;

            txtAddress_gridContact.Clear();
            cmbCounty_gridContact.SelectedItem = null;
            cmbCounty_gridContact.Text = string.Empty;
            txtState_gridContact.Clear();
            txtZipCode_gridContact.Clear();

            txtHomePhone_gridContact.Clear();
            txtWorkPhone_gridContact.Clear();
            txtCellPhone_gridContact.Clear();
            txtFax_gridContact.Clear();
            txtEmail_gridContact.Clear();

            // Hiding the grid.
            gridContact.Visibility = Visibility.Hidden;
        }

        private void btnAddGroup_Click(object sender, RoutedEventArgs e)
        {
            gridGroup.Visibility = Visibility.Visible;
        }

        private async void btnDeleteGroup_Click(object sender, RoutedEventArgs e)
        {
            ContactGroup selitem;
            if ((selitem = lstContactGroups.SelectedItem as ContactGroup) != null)
            {
                btnDeleteGroup.IsEnabled = false;

                try
                {
                    // Deleting from the database.
                    using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                    {
                        cmd.CommandText = "DELETE FROM contact_groups WHERE id = @id";
                        cmd.Parameters.AddWithValue("id", selitem.ID);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Removing from the list.
                    m_contactGroups.Remove(selitem);

                    // Reloading the contacts list.
                    await LoadContacts();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected contact group.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteGroup.IsEnabled = true;
                }
            }
            else
            {
                m_mainwindow.ShowMessage("No item selected", "Please select a contact group from the list first!");
            }
        }

        private async void btnOK_gridGroup_Click(object sender, RoutedEventArgs e)
        {
            btnOK_gridGroup.IsEnabled = false;
            btnCancel_gridGroup.IsEnabled = false;

            try
            {
                // Checking the user inputs.
                if (string.IsNullOrEmpty(txtGroupName_gridGroup.Text))
                {
                    throw new Exception("Group name is null");
                }

                ContactGroup group = new ContactGroup();
                group.ID = Randomizer.RandomInt(0, int.MaxValue);
                group.Name = txtGroupName_gridGroup.Text.ToUpper();

                // Adding to the database.
                using (SQLiteCommand cmd = new SQLiteCommand(m_dbConn))
                {
                    cmd.CommandText = "INSERT INTO contact_groups(id, value) VALUES(@id, @value)";
                    cmd.Parameters.AddWithValue("id", group.ID);
                    cmd.Parameters.AddWithValue("value", group.Name);
                    await cmd.ExecuteNonQueryAsync();
                }

                // Adding to the list.
                m_contactGroups.Add(group);

                // Done.
                btnCancel_gridGroup_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes. Please try again!\nDetails: " + ex.Message);
            }
            finally
            {
                btnOK_gridGroup.IsEnabled = true;
                btnCancel_gridGroup.IsEnabled = true;
            }
        }

        private void btnCancel_gridGroup_Click(object sender, RoutedEventArgs e)
        {
            // Clearing user inputs.
            txtGroupName_gridGroup.Clear();

            // Hiding the grid.
            gridGroup.Visibility = Visibility.Hidden;
        }
    }
}
