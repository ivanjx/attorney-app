﻿using attorney_app.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attorney_app.Commons
{
    public static class DatabaseAccess
    {
        public static readonly string TEMP_DIR = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "AttorneyApp");


        static DatabaseAccess()
        {
            // Creating temp dir.
            if (!Directory.Exists(TEMP_DIR))
            {
                Directory.CreateDirectory(TEMP_DIR);
            }

            // Creating the storage dir.
            string storageDir = Path.Combine(TEMP_DIR, "storage");
            if (!Directory.Exists(storageDir))
            {
                Directory.CreateDirectory(storageDir);
            }
        }

        public static Stream GetFileStream(string fileId, FileMode mode)
        {
            string filePath = Path.Combine(TEMP_DIR, "storage", fileId);

            if (File.Exists(filePath))
            {
                return File.Open(filePath, mode);
            }

            return null;
        }

        public static async Task<string> UploadFile(SQLiteConnection dbConn, string originalPath, string fileName)
        {
            // Generating new file ID.
            string id = await Randomizer.RandomStringAsync(32);
            string targetPath = Path.Combine(TEMP_DIR, "storage", id);

            // Copying the file.
            using (Stream input = File.Open(originalPath, FileMode.Open))
            using (Stream output = File.Open(targetPath, FileMode.Create))
            {
                await input.CopyToAsync(output);
            }

            // Saving to the database.
            using (SQLiteCommand cmd = new SQLiteCommand(dbConn))
            {
                cmd.CommandText = "INSERT INTO files(id, original_name) VALUES(@fileId, @originalName)";
                cmd.Parameters.AddWithValue("fileId", id);
                cmd.Parameters.AddWithValue("originalName", Path.GetFileName(fileName));
                await cmd.ExecuteNonQueryAsync();
            }

            // Returning the file id.
            return id;
        }

        public static async Task DeleteFile(SQLiteConnection dbConn, string fileId)
        {
            // Removing from the database.
            using (SQLiteCommand cmd = new SQLiteCommand(dbConn))
            {
                cmd.CommandText = "DELETE FROM files WHERE id = @fileId";
                cmd.Parameters.AddWithValue("fileId", fileId);
                await cmd.ExecuteNonQueryAsync();
            }

            // Deleting the file.
            string targetPath = Path.Combine(TEMP_DIR, "storage", fileId);
            File.Delete(targetPath);
        }
    }
}
